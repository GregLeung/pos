var express = require('express');
var router = express.Router();
const ProductModel = require('../models/Product');
const POModel = require('../models/PurchaseOrder');
const VendorModel = require('../models/Vendor');
const CategoryModel = require('../models/Category');
const BrandModel = require('../models/Brand');
const SalesRecordModel = require('../models/SalesRecord');
const dataManipulation = require('../utils/dataManipulation');
const mongoose = require('mongoose');
const comparators = require('../utils/comparators');
const CsvHelper = require('../utils/CsvHelper');

router.get('/vendors', function (req, res) {
  VendorModel.find({}, (err, docs) => {
    if (err) {
      res.status(500);
      res.json(err);
    } else {
      return res.json(docs.sort(comparators.nameComparator));
    }
  });
});

router.get('/brands', function (req, res) {
  BrandModel.find({}, (err, docs) => {
    if (err) {
      res.status(500);
      res.json(err);
    } else {
      return res.json(docs.sort(comparators.nameComparator));
    }
  });
});

router.get('/categories', function (req, res) {
  CategoryModel.find({}, (err, docs) => {
    if (err) {
      res.status(500);
      res.json(err);
    } else {
      return res.json(docs.sort(comparators.nameComparator));
    }
  });
});

router.get('/product/:identifier', function (req, res) {
  let productIdentifier = decodeURI(req.params.identifier);
  ProductModel.findOne({ $or: [{ barcode: productIdentifier }, { productCode: productIdentifier }] })
    .populate('brand')
    .populate('category')
    .exec((error, doc) => {
      if (error) {
        res.status(500);
        return res.json(error);
      }
      if (doc == null) {
        res.status(401);
        return res.json({ code: 5000, msg: `Barcode/ProductCode: ${productIdentifier} not found` });
      }
      return res.json(doc);
    });
});

router.post('/products', async function (req, res) {
  try{
    let result = await getProducts(req.body.description, req.body.category._id, req.body.brand._id);
    console.log(result);
    res.status(200);
    return res.json(result);
  }catch(error){
    console.log(error);
    res.status(500);
    return res.json({});
  }
});

router.post('/purchaseOrder/byDateRange', function (req, res) {
  let { startDate, endDate } = req.body;
  startDate = new Date(startDate);
  endDate = new Date(endDate);
  POModel.aggregate([
    { $match: { createdAt: { $gte: startDate, $lte: endDate } } },
    { $lookup: { from: 'Products', localField: 'items._id', foreignField: '_id', as: 'temp' } },
    { $lookup: { from: 'Vendors', localField: 'vendor', foreignField: '_id', as: 'vendor' } },
    { $unwind: '$vendor' },
  ]).exec(function (error, resp) {
    let result = dataManipulation.packagePOResp(resp);
    res.json({ result });
  });
});

router.post('/salesRecords/byCriterias', function (req, res) {
  let { startDate, endDate } = req.body;
  startDate = new Date(startDate);
  endDate = new Date(endDate);
  SalesRecordModel.aggregate([
    { $match: { $and: [{ createdAt: { $gte: startDate, $lte: endDate } }, { recordType: req.body.type }] } },
    { $lookup: { from: 'Products', localField: 'items._id', foreignField: '_id', as: 'itemsTemp' } },
    { $project: { 'payments._id': 0 } },
  ]).exec(function (error, resp) {
    res.json(dataManipulation.packageSalesResp(resp));
  });
});

router.post('/report/byTimeRange', async function (req, res) {
  let { startDate, endDate } = req.body;
  startDate = new Date(startDate);
  endDate = new Date(endDate);
  const match = {
    $match: {
      $and: [
        { createdAt: { $gte: startDate, $lte: endDate } },
        { recordType: { $nin: ['quotation'] } },
        { isDeleted: false },
      ],
    },
  };
  const group = {
    $group: {
      _id: '$recordType',
      payments: { $push: '$actualPayments' },
      items: { $push: '$items' },
      count: { $sum: 1 },
      totals: { $push: '$total' },
    },
  };
  const aggr = [];
  aggr.push(match);
  aggr.push(group);

  let result = await SalesRecordModel.aggregate(aggr);
  result = dataManipulation.packageSalesReportResp(result);
  res.json(result);
});

router.get('/report/getCsv', async function (req, res){
  let {startDate, endDate} = req.query;
  startDate = new Date(parseInt(startDate));
  endDate = new Date(parseInt(endDate));
  const match = {
    $match: {
      $and: [
        { createdAt: { $gte: startDate, $lte: endDate } },
        { recordType: { $nin: ['quotation'] } },
        { isDeleted: false },
      ],
    },
  };

  const aggr = [];
  aggr.push(match);
  let result = await SalesRecordModel.aggregate(aggr);
  let csv = CsvHelper.getSalesRecordsCsv(result);
  res.setHeader('Content-disposition', 'attachment; filename=salesRecords.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csv);
})

router.get('/products/getCsv', async function (req, res){
  try{
    let result = await getProducts(req.query.description, req.query.category, req.query.brand);
    console.log(result);
    let csv = CsvHelper.getProductListCsv(result);
    res.setHeader('Content-disposition', 'attachment; filename=productList.csv');
    res.set('Content-Type', 'text/csv');
    res.status(200).send(csv);
  }catch(error){
    console.log(error);
    res.status(500);
    return res.json({});
  }
  
})

async function getProducts(description, categoryId, brandId){
  let textSearchStage;
  let brandSearchStage;
  let categorySearchStage;
  if (description) {
    textSearchStage = { $match: { $text: { $search: description } } };
  }
  if (categoryId) {
    categorySearchStage = { $match: { category: new mongoose.Types.ObjectId(categoryId) } };
  }
  if (brandId) {
    brandSearchStage = { $match: { brand: new mongoose.Types.ObjectId(brandId) } };
  }

  let brandLookupStage = { $lookup: { from: 'Brands', localField: 'brand', foreignField: '_id', as: 'brand' } };
  let catLookupStage = { $lookup: { from: 'Categories', localField: 'category', foreignField: '_id', as: 'category' } };
  let pipes = [];
  if (textSearchStage) {
    pipes.push(textSearchStage);
  }
  if (brandSearchStage) {
    pipes.push(brandSearchStage);
  }
  if (categorySearchStage) {
    pipes.push(categorySearchStage);
  }

  pipes.push(brandLookupStage);
  pipes.push(catLookupStage);
  pipes.push({ $unwind: '$category' });
  pipes.push({ $unwind: '$brand' });

  return result = await ProductModel.aggregate(pipes);
}

module.exports = router;
