var express = require('express');
var router = express.Router();
const SalesRecordModel = require('../models/SalesRecord');
const ProductModel = require('../models/Product');
const QueryHelper = require('../utils/queryHelper');
const DataManipulation = require('../utils/dataManipulation');
const IDGenerator = require('../utils/IDGenerator');
const ProductHelper = require('../utils/ProductHelper');
const Validator = require('../utils/Validator');

router.post('', async function (req, res) {
  if (!Validator.validateRecord(req.body)) {
    res.status(401);
    res.json({ msg: 'Invalid Payment', code: 401 });
    return;
  }
  let recordType = req.body.recordType;
  let actualPayments = [];
  let payments = req.body.payments;
  if (recordType === 'return') {
    req.body = DataManipulation.negateAmountForReturnRecord(req.body);
    actualPayments = DataManipulation.getArrayDeepCopy(payments).filter((ele) => ele.amt != null);
  } else {
    let payable = DataManipulation.getPayable(req.body.items);
    actualPayments = DataManipulation.getActualPayments(payments, payable);
  }
  req.body.actualPayments = actualPayments;
  try {
    //create salesRecord
    req.body._id = await IDGenerator.generateRecordID(req.body.recordType);
    let salesRecord = await SalesRecordModel.create([req.body]);
    let writeQueries = [];
    switch (req.body.recordType) {
      case 'invoice':
        //change stock level
        writeQueries = QueryHelper.getItemAdjustmentQueries(req.body.items, 'minus');
        await ProductModel.bulkWrite(writeQueries);
        break;
      case 'return':
        //change stock level
        writeQueries = QueryHelper.getItemAdjustmentQueries(req.body.items, 'add');
        await ProductModel.bulkWrite(writeQueries);
    }

    const searchByIdStage = { $match: { _id: salesRecord[0]._id } };
    const lookupProductStage = {
      $lookup: { from: 'Products', localField: 'items._id', foreignField: '_id', as: 'itemsTemp' },
    };
    const pipe = [];
    pipe.push(searchByIdStage);
    pipe.push(lookupProductStage);

    let result = await SalesRecordModel.aggregate(pipe);
    DataManipulation.packageSalesResp(result);
    res.status(201);
    res.json(result[0]);
  } catch (error) {
    console.log(error);
    res.json('failed');
  }
});

router.post('/convert', async function (req, res) {
  try {
    req.body._id = await IDGenerator.generateRecordID(req.body.recordType);
    let oriRecord = await SalesRecordModel.findOne({ _id: req.body.convertedFrom });
    let depositPayments = oriRecord.payments;
    let depositActualPayments = oriRecord.actualPayments;

    let convertedInvoicePayments = req.body.payments;

    let payable = DataManipulation.getPayable(req.body.items);
    let paid = depositActualPayments
      .map((ele) => ele.amt)
      .reduce((accumulator, currentValue) => {
        return accumulator + currentValue;
      }, 0);
    let amountDue = payable - paid;
    let actualPayments = [];
    if (amountDue > 0) {
      //get this payment
      convertedInvoicePayments.forEach((ele) => {
        if (ele.amt == null) {
          return;
        }
        const paymentIndex = depositPayments.findIndex((depositPayment) => depositPayment.name === ele.name);
        if (paymentIndex != -1) {
          ele.amt -= depositPayments[paymentIndex].amt;
        }
      });
      //deep copied
      actualPayments = DataManipulation.getActualPayments(convertedInvoicePayments, amountDue);
    } else {
      convertedInvoicePayments.forEach((ele) => (ele.amt = null));
    }
    req.body.actualPayments = actualPayments;
    req.body.total = amountDue;
    req.body.deposit = paid;
    await ProductModel.bulkWrite(QueryHelper.getItemAdjustmentQueries(req.body.items, 'minus'));
    let salesRecord = await SalesRecordModel.create([req.body]);
    let updateResponse = await SalesRecordModel.updateOne(
      { _id: req.body.convertedFrom },
      { convertedTo: salesRecord[0]._id, fullFilled: true }
    );
    let invoices = await ProductHelper.getSalesRecord(salesRecord[0]._id);
    DataManipulation.packageSalesResp(invoices);
    res.status(201);
    res.json(invoices[0]);
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json('failed');
  }
});

router.put('', async function (req, res) {
  console.log(req.body);
  if (req.body.isDeleted == true) {
    switch (req.body.recordType) {
      case 'invoice':
        writeQueries = QueryHelper.getItemAdjustmentQueries(req.body.items, 'add');
        await ProductModel.bulkWrite(writeQueries);
        if (req.body.convertedFrom) {
          await SalesRecordModel.updateOne({ _id: req.body.convertedFrom }, { $set: { fullFilled: false } });
        }
        break;
    }
  }
  try {
    let result = await SalesRecordModel.updateOne({ _id: req.body._id }, req.body);
    res.status(201);
    return res.json(result);
  } catch (error) {
    console.log(error);
    res.json('failed');
  }
});

module.exports = router;
