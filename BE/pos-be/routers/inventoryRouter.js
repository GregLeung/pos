var express = require('express');
var router = express.Router();
const ProductModel = require('../models/Product');
const PurchaseOrderModel = require('../models/PurchaseOrder');
const VendorModel = require('../models/Vendor');
const BrandModel = require('../models/Brand');
const CategoryModel = require('../models/Category');
const ProductIDModel = require('../models/ProductID');
const QueryHelper = require('../utils/queryHelper');
const mongoose = require('mongoose');
const ProductIDHelper = require('../utils/ProductIDHelper');

router.post('/purchaseOrder', async function (req, res) {
  console.log(req.body);
  try {
    let po = await PurchaseOrderModel.create([req.body]);
  } catch (error) {
    res.status(500);
    res.json(error);
    return;
  }
  let writeQueries = [];
  writeQueries = QueryHelper.getItemAdjustmentQueries(req.body.items, 'add');
  await ProductModel.bulkWrite(writeQueries);
  res.status(201);
  res.json();
});

router.post('/vendor', async function (req, res) {
  const doc = new VendorModel({
    name: req.body.name,
    phoneNumbers: req.body.phoneNumbers,
    faxNumbers: req.body.faxNumbers,
    addresses: req.body.addresses,
    emails: req.body.emails,
    paymentPeriodDays: req.body.paymentPeriodDays,
  });
  await doc.save();
  const vendors = await VendorModel.find();
  res.status(201);
  res.json(vendors);
});

router.put('/vendor', function (req, res) {
  VendorModel.updateOne({ _id: req.body._id }, req.body, (resp, error) => {
    res.status(201);
    return res.json(resp);
  });
});

router.post('/brand', function (req, res) {
  console.log(req.body);
  req.body.name = req.body.name.trim().toUpperCase();
  BrandModel.create(req.body, (err, resp) => {
    if (err) {
      res.status(400);
      res.json(err);
      return;
    }
    res.status(201);
    return res.json(resp);
  });
});

router.put('/brand', function (req, res) {
  console.log(req.body);
  req.body.name = req.body.name.trim().toUpperCase();
  BrandModel.updateOne({ _id: req.body._id }, req.body, (err, resp) => {
    res.status(201);
    return res.json(resp);
  });
});

router.post('/category', function (req, res) {
  req.body.name = req.body.name.trim().toUpperCase();
  CategoryModel.create(req.body, (err, resp) => {
    if (err) {
      res.status(400);
      res.json(err);
      return;
    }
    res.status(201);
    return res.json(resp);
  });
});

router.put('/category', function (req, res) {
  console.log(req.body);
  req.body.name = req.body.name.trim().toUpperCase();
  CategoryModel.updateOne({ _id: req.body._id }, req.body, (err, resp) => {
    res.status(201);
    return res.json(resp);
  });
});

router.delete('/vendor/:id', function (req, res) {
  console.log();
  VendorModel.deleteOne({ _id: new mongoose.Types.ObjectId(req.params.id) }, (err, resp) => {
    if (err) {
      console.log(err);
      res.status(500);
      res.json(err);
    }
    res.status(204);
    res.json(resp);
  });
});

router.delete('/purchaseOrder/:id', async function (req, res) {
  try {
    const po = await PurchaseOrderModel.findOne({ _id: new mongoose.Types.ObjectId(req.params.id) });
    writeQueries = QueryHelper.getItemAdjustmentQueries(po.items, 'minus');
    await ProductModel.bulkWrite(writeQueries);
    let result = await PurchaseOrderModel.deleteOne({ _id: new mongoose.Types.ObjectId(req.params.id) });
    res.status(204);
    res.json(result);
  } catch (err) {
    console.log(err);
    res.status(500);
    res.json(err);
  }
});

router.post('/product', async function (req, res) {
  const productCodes = req.body.productCode;
  if (req.body.qty == null) {
    req.body.qty = 0;
  }
  let ids = [];
  productCodes.forEach((ele) => {
    ids.push(ele);
  });
  ids.push(req.body.barcode);

  const result = await ProductIDHelper.checkIdsAreUnique(ids);

  console.log(result);

  if (result.length != 0) {
    res.status(500);
    res.json({ keyValue: result.map((ele) => ele._id), code: 11000 });
    return;
  }

  ids = ProductIDHelper.getMapToIdObjects(ids);

  ProductIDModel.insertMany(ids, (error, data) => {
    if (error) {
      res.status(500);
      res.json(error);
      return;
    }
    ProductModel.create(req.body, (error, data) => {
      if (error) {
        res.status(500);
        res.json(error);
        return;
      }
      res.status(201);
      res.json(data);
    });
  });
});
router.put('/product', async function (req, res) {
  const newIds = req.body.newIds;
  if (req.body.qty == null) {
    req.body.qty = 0;
  }
  const idsToRemove = req.body.idToRemove;
  console.log(req.body);
  const result = await ProductIDHelper.checkIdsAreUnique(newIds);

  if (result.length != 0) {
    res.status(500);
    res.json({ keyValue: result.map((ele) => ele._id), code: 11000 });
    return;
  }

  try {
    if (newIds.length > 0) {
      await ProductIDModel.insertMany(ProductIDHelper.getMapToIdObjects(newIds));
    }
    if (idsToRemove.length > 0) {
      await ProductIDModel.deleteMany({ _id: { $in: idsToRemove } });
    }
    let result = await ProductModel.updateOne({ _id: req.body._id }, req.body);
    res.status(201);
    return res.json(result);
  } catch (error) {
    res.status(500);
    return res.json(error);
  }
});
module.exports = router;
