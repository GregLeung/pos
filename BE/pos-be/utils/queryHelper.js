const mongoose = require('mongoose');

function getItemAdjustmentQueries(items, operation) {
  let multiplier = 1;
  switch (operation) {
    case 'minus':
      multiplier = -1;
      break;
  }
  let writeQueries = [];
  items.forEach((item) => {
    let updateDoc = {
      updateOne: {
        filter: { _id: new mongoose.Types.ObjectId(item._id) },
        update: { $inc: { qty: item.qty * multiplier }, $set: { sn: item.sn } },
      },
    };
    writeQueries.push(updateDoc);
  });
  return writeQueries;
}

module.exports.getItemAdjustmentQueries = getItemAdjustmentQueries;
