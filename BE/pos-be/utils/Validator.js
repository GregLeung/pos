function validateRecord(record) {
  let recordType = record.recordType;
  switch (recordType) {
    case 'invoice':
      let payments = JSON.parse(JSON.stringify(record.payments.filter((ele) => ele.amt != null)));
      let totalPayment = payments
        .map((ele) => ele.amt)
        .reduce((accumulator, currentValue) => {
          return accumulator + currentValue;
        }, 0);
      return totalPayment >= record.total ? true : false;
  }
  return true;
}

module.exports.validateRecord = validateRecord;
