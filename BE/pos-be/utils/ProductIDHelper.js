const ProductID = require('../models/ProductID');

function getMapToIdObjects(ids) {
  ids = ids.map(function (ele) {
    return { _id: ele };
  });
  return ids;
}

async function checkIdsAreUnique(ids) {
  let result = await ProductID.find({ _id: { $in: ids } });
  return result;
}

module.exports.checkIdsAreUnique = checkIdsAreUnique;
module.exports.getMapToIdObjects = getMapToIdObjects;
