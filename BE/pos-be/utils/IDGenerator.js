const IDCounterHelper = require('./IDCounterHelper');

async function generateRecordID(recordType) {
  let id = await IDCounterHelper.getNextSequence(recordType);
  const date = new Date();
  const type = recordType.toUpperCase().charAt(0);
  return type + date.getFullYear() + date.getMonth() + date.getDate() + id;
}

module.exports.generateRecordID = generateRecordID;
