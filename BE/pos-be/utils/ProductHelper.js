const SalesRecordModel = require('../models/SalesRecord');

async function getSalesRecord(salesRecordId) {
  const searchByIdStage = { $match: { _id: salesRecordId } };
  const lookupProductStage = {
    $lookup: { from: 'Products', localField: 'items._id', foreignField: '_id', as: 'itemsTemp' },
  };
  const pipe = [];
  pipe.push(searchByIdStage);
  pipe.push(lookupProductStage);

  let result = await SalesRecordModel.aggregate(pipe);
  return result;
}

module.exports.getSalesRecord = getSalesRecord;
