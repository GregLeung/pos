function nameComparator(objA, objB) {
  const nameA = objA.name;
  const nameB = objB.name;
  if (nameA < nameB) {
    return -1;
  }
  if (nameB > nameA) {
    return 1;
  }
  return 0;
}

function itemComparator(itemA, itemB) {
  const barcodeA = itemA._id;
  const barcodeB = itemB._id;
  if (barcodeA < barcodeB) {
    return -1;
  }
  if (barcodeA > barcodeB) {
    return 1;
  }
  return 0;
}

function salesRecordComparator(recordA, recordB) {
  const recordAType = recordA._id;
  const recordBType = recordB._id;
  if (recordAType < recordBType) {
    return -1;
  }
  if (recordAType > recordBType) {
    return 1;
  }
  return 0;
}

module.exports.itemComparator = itemComparator;
module.exports.salesRecordComparator = salesRecordComparator;
module.exports.nameComparator = nameComparator;
