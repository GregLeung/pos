const comparators = require('./comparators');

function getPayable(items) {
  let payable = items
    .map((ele) => ele.qty * ele.adjustedPrice)
    .reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);
  return payable;
}

function getArrayDeepCopy(arr) {
  return JSON.parse(JSON.stringify(arr));
}

function getActualPayments(payments, payable) {
  let actualPayments = [];
  actualPayments = JSON.parse(JSON.stringify(payments.filter((ele) => ele.amt != null)));
  let temp = 0;
  temp = actualPayments
    .map((ele) => ele.amt)
    .reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);

  if (temp >= payable) {
    actualPayments.forEach((ele) => {
      if (ele.name == 'CASH') {
        return;
      }
      payable -= ele.amt;
    });
  } else {
    return actualPayments;
  }
  let cashIndex = actualPayments.findIndex((element) => element.name === 'CASH');
  if (cashIndex != -1) {
    actualPayments[cashIndex].amt = payable;
  }
  return actualPayments;
}

function negateAmountForReturnRecord(requestBody) {
  requestBody.payments = requestBody.payments.map((ele) => {
    const temp = ele;
    temp.amt = temp.amt == undefined ? null : temp.amt * -1;
    return temp;
  });
  return requestBody;
}

function packagePOResp(aggregatedPOs) {
  aggregatedPOs.forEach((element) => {
    element.items.sort(comparators.itemComparator);
    element.temp.sort(comparators.itemComparator);
    let products = [];
    for (let i = 0; i < element.items.length; i++) {
      let temp = {};
      console.log(element);
      temp.name = element.temp[i].name;
      temp.desc = element.temp[i].desc;
      temp.qty = element.items[i].qty;
      temp.price = element.items[i].price;
      temp.barcode = element.items[i].barcode;
      products.push(temp);
    }
    delete element.temp;
    element.items = products;
  });
  return aggregatedPOs;
}

function packageSalesResp(aggregatedSalesRecords) {
  aggregatedSalesRecords.forEach((element) => {
    element.items.sort(comparators.itemComparator);
    element.itemsTemp.sort(comparators.itemComparator);
    let products = [];
    for (let i = 0; i < element.items.length; i++) {
      let temp = {};
      temp._id = element.itemsTemp[i]._id;
      temp.name = element.itemsTemp[i].name;
      temp.desc = element.itemsTemp[i].desc;
      temp.adjustedPrice = element.items[i].adjustedPrice;
      temp.price = element.itemsTemp[i].price;
      temp.barcode = element.itemsTemp[i].barcode;
      temp.qty = element.items[i].qty;
      temp.originalQty = element.itemsTemp[i].qty;
      temp.cost = element.items[i].cost;
      products.push(temp);
    }
    delete element.itemsTemp;
    element.items = products;
  });
  return aggregatedSalesRecords;
}

function packageSalesReportResp(aggregatedSalesRecords) {
  let sumPaymentMap = new Map();
  let profit = 0;
  aggregatedSalesRecords.forEach((ele) => {
    if (ele._id != 'order') {
      cost = getCost(ele._id, ele.items.flat());
      amount = getSalesRecordAmount(ele._id, ele.items.flat());
      profit += amount - cost;
    }
    let salesRecordPaymentMap = new Map();
    const payments = ele.payments.flat();
    salesRecordPaymentMap = groupByPaymentMethod(salesRecordPaymentMap, payments);
    sumPaymentMap = groupByPaymentMethod(sumPaymentMap, payments);
    ele.payments = Array.from(salesRecordPaymentMap, ([name, value]) => ({ name, value }));
    ele.subTotal = ele.payments.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.value.amt;
    }, 0);
  });
  const result = {
    summary: {
      payments: Array.from(sumPaymentMap, ([name, value]) => ({ name, value })),
      count: aggregatedSalesRecords.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.count;
      }, 0),
      subTotal: Array.from(sumPaymentMap, ([name, value]) => ({ name, value })).reduce((accumulator, currentValue) => {
        return accumulator + currentValue.value.amt;
      }, 0),
      profit: profit,
    },
    salesRecords: aggregatedSalesRecords.sort(comparators.salesRecordComparator),
  };
  return result;
}

function groupByPaymentMethod(paymentMap, payments) {
  payments.forEach((ele) => {
    if (ele.amt === 0) {
      return;
    }
    const paymentMethod = ele.name;
    paymentMap.has(paymentMethod)
      ? paymentMap.set(paymentMethod, {
          count: paymentMap.get(paymentMethod).count + 1,
          amt: paymentMap.get(paymentMethod).amt + ele.amt,
        })
      : paymentMap.set(paymentMethod, { count: 1, amt: ele.amt });
  });

  return paymentMap;
}

function getSalesRecordAmount(recordType, items) {
  let amount = items
    .map((item) => item.qty * item.adjustedPrice)
    .reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);
  return recordType === 'return' ? amount * -1 : amount;
}

function getCost(recordType, items) {
  let cost = items
    .map((item) => item.qty * item.cost)
    .reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);
  return recordType === 'return' ? cost * -1 : cost;
}

module.exports.packagePOResp = packagePOResp;
module.exports.packageSalesResp = packageSalesResp;
module.exports.packageSalesReportResp = packageSalesReportResp;
module.exports.negateAmountForReturnRecord = negateAmountForReturnRecord;
module.exports.getActualPayments = getActualPayments;
module.exports.getPayable = getPayable;
module.exports.getArrayDeepCopy = getArrayDeepCopy;
module.exports.getCost = getCost;