const IDCounterModel = require('../models/IDCounter');

async function getNextSequence(recordType) {
  let iDCounter = await IDCounterModel.findOneAndUpdate(recordType, { $inc: { seq: 1 } });
  return iDCounter.seq;
}

module.exports.getNextSequence = getNextSequence;
