const { parse } = require('json2csv');
const dataManipulation = require('../utils/dataManipulation');

function getSalesRecordsCsv(records) {
  try {
    const fields = [
      { label: 'Id', value: '_id' },
      { label: 'Record Type', value: 'recordType' },
      { label: 'Converted From', value: 'convertedFrom' },
      { label: 'Created At', value: 'createdAt' },
      { label: 'Salesman', value: 'salesman' },
      { label: 'Cash', value: 'CASH' },
      { label: 'EPS', value: 'EPS' },
      { label: 'Visa', value: 'VISA' },
      { label: 'Master', value: 'MASTER' },
      { label: 'Union Pay', value: 'UNION PAY' },
      { label: 'AE', value: 'AE' },
      { label: 'P. Card', value: 'P. Card' },
      { label: 'CR Note', value: 'CR Note' },
      { label: 'Bank Transfer', value: 'Bank Tsf.' },
      { label: 'Deposit', value: 'deposit' },
      { label: 'SubTotal', value: 'subTotal' },
      { label: 'Cost', value: 'cost' },
    ];
    records.forEach((record) => {
      record.payments.forEach((payment) => {
        if (payment.amt == 0) return;
        record[payment.name] = payment.amt;
      });
      record.subTotal = record.recordType == 'return' ? record.total * -1 : record.total;
      record.createdAt = record.createdAt.toLocaleString('en-GB', { timeZone: 'Asia/Hong_Kong', hour12: false });
      let cost = dataManipulation.getCost(record.recordType, record.items);
      record.cost = (record.recordType == 'invoice' && record.convertedFrom) || record.recordType == 'order' ? 0 : cost;
    });
    const csv = parse(records, { fields });
    return csv;
  } catch (err) {
    console.log(err);
  }
}

function getProductListCsv(products) {
  try {
    const fields = [
      { label: 'Barcode', value: 'barcode' },
      { label: 'Product Code', value: 'productCode' },
      { label: 'Name', value: 'name' },
      { label: 'Description', value: 'desc' },
      { label: 'Category', value: 'category' },
      { label: 'Brand', value: 'brand' },
      { label: 'Quantity', value: 'qty' },
      { label: 'Price', value: 'price' },
      { label: 'Cost', value: 'cost' },
    ];
    products.forEach((product) => {
      console.log(product);
      product.category = product.category.name;
      product.brand = product.brand.name;
    });
    const csv = parse(products, { fields });
    return csv;
  } catch (err) {
    console.log(err);
  }
}

module.exports.getSalesRecordsCsv = getSalesRecordsCsv;
module.exports.getProductListCsv = getProductListCsv;
