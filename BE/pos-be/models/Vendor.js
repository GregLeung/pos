const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VendorSchema = new Schema(
  {
    name: String,
    phoneNumbers: [String],
    faxNumbers: [String],
    addresses: [String],
    emails: [String],
    paymentPeriodDays: Number,
  },
  { timestamps: true }
);

module.exports = mongoose.model('Vendor', VendorSchema, 'Vendors');
