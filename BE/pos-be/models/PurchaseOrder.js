const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PurchaseOrderSchema = new Schema(
  {
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendors' },
    vendorRefNum: { type: String, unique: true },
    vendorOrderDate: Date,
    purchaser: String,
    remark: String,
    items: [{ barcode: String, price: Number, qty: Number }],
  },
  { timestamps: true }
);

module.exports = mongoose.model('PurchaseOrder', PurchaseOrderSchema, 'PurchaseOrders');
