const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//salesRecordIDCounter
const IDCounterSchema = new Schema({
  _id: String,
  seq: Number,
});

module.exports = mongoose.model('IDCounter', IDCounterSchema, 'IDCounters');
