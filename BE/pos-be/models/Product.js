const mongoose_fuzzy_searching = require('mongoose-fuzzy-searching');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  barcode: { type: String },
  productCode: [String],
  name: { type: String },
  desc: { type: String },
  brand: { type: Schema.Types.ObjectId, ref: 'Brand' },
  category: { type: Schema.Types.ObjectId, ref: 'Category' },
  qty: { type: Number, default: 0 },
  price: Number,
  cost: Number,
  sn: [String],
});

ProductSchema.plugin(mongoose_fuzzy_searching, { fields: ['name', 'desc'] });

module.exports = mongoose.model('Product', ProductSchema, 'Products');
