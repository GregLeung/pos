const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SalesRecordSchema = new Schema(
  {
    _id: String,
    recordType: String,
    total: Number,
    salesman: String,
    fullFilled: Boolean,
    convertedTo: String,
    convertedFrom: String,
    deposit: { type: Number, default: null },
    payments: [{ amt: Number, name: String }],
    actualPayments: [{ amt: Number, name: String }],
    items: [{ barcode: String, price: Number, adjustedPrice: Number, qty: Number, cost: Number }],
    isDeleted: { type: Boolean, default: false },
  },
  { timestamps: true }
);

module.exports = mongoose.model('SalesRecord', SalesRecordSchema, 'SalesRecords');
