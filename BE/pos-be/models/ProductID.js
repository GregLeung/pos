const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//
const ProductIDSchema = new Schema({
  _id: { type: String },
});

module.exports = mongoose.model('ProductID', ProductIDSchema, 'ProductIDs');
