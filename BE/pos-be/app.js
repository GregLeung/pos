const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const inventoryRouter = require('./routers/inventoryRouter');
const salesRecordRouter = require('./routers/salesRecordRouter');
const searchRouter = require('./routers/searchRouter');
const mongoose = require('mongoose');

const HOST = '0.0.0.0';
const PORT = 3000;

const winston = require('winston');
const expressWinston = require('express-winston');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader('Access-Control-Allow-Headers', '*');
  next();
});

const url = 'mongodb://database:27017/pos';
mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const db = mongoose.connection;
db.once('open', () => {
  console.log('Database connected:', url);
});

db.on('error', (err) => {
  console.error('connection error:', err);
});
expressWinston.requestWhitelist.push('body');

app.use(
  expressWinston.logger({
    level: 'info',
    transports: [new winston.transports.Console(), new winston.transports.File({ filename: './logs/be.log' })],
    format: winston.format.combine(
      winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
      winston.format.colorize(),
      winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message} RequestBody:${JSON.stringify(info.meta.req.body)}`)
      ),
    meta: true,
    msg: 'HTTP {{req.method}} {{req.url}}',
  })
);

app.use('/api/inventory', inventoryRouter);
app.use('/api/salesRecord', salesRecordRouter);
app.use('/api/search', searchRouter);

app.listen(PORT, HOST, () => {
  console.log(`PosBE listening at http://${HOST}:${PORT}`);
});
