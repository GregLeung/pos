export const environment = {
  production: true,
  // Hung test machine url
  // baseUrl: 'http://192.168.0.162/api/',
  // Hung prod machine url
  baseUrl: 'http://192.168.0.157/api/',
  // Ming sydney homeurl
  // baseUrl: 'http://192.168.1.2/api/',
};
