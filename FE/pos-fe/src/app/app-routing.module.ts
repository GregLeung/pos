import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './components/report/report.component';
import { SalesComponent } from './components/sales/sales.component';
import { InventoryComponent } from 'app/components/inventory/inventory.component';
import { ManagementComponent } from 'app/components/management/management.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'report', component: ReportComponent },
  { path: 'inventory', component: InventoryComponent },
  { path: 'sales', component: SalesComponent },
  { path: 'management', component: ManagementComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
