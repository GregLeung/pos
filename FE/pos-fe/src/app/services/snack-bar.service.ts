import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  durationInSeconds = 5;

  constructor(private snackBar: MatSnackBar) {}

  showValidationMsg(msg) {
    this.snackBar.open(msg, 'OK', { verticalPosition: 'top', duration: 5000, panelClass: ['multiLinedSnack'] });
  }

  openSnackBar(action, event) {
    switch (action) {
      case 'Error':
        this.handleError(event.error);
        break;
      case 'Success':
        this.handleSuccess(event.status);
        break;
    }
  }

  handleError(error) {
    let message = '';
    switch (error.code) {
      case 11000:
        message = `Duplicate entry: ${JSON.stringify(error.keyValue)}`;
        break;
      case 5000:
        message = error.msg;
        break;
      case 401:
        message = error.msg;
    }
    this.snackBar.open(message, 'OK', { duration: 7000 });
  }

  handleSuccess(status) {
    let message = '';
    switch (status) {
      case 201:
        message = 'Successful';
        break;
      case 204:
        message = 'Deleted';
        break;
    }
    this.snackBar.open(message, 'OK', { duration: 7000 });
  }
}
