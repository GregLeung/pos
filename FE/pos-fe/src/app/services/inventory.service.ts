import { DataShareService } from 'app/services/data-share.service';
import { Product } from 'app/models/Product';
import { Vendor } from 'app/models/Vendor';
import { Category } from 'app/models/Category';
import { Brand } from 'app/models/Brand';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PurchaseOrder } from 'app/models/PurchaseOrder';

@Injectable({
  providedIn: 'root',
})
export class InventoryService {
  catOptions: Category[] = [];
  brandOptions: Brand[] = [];
  vendorOptions: Vendor[] = [];

  constructor(private http: HttpClient, private dataShareService: DataShareService) {
    this.getVendors().subscribe((res) => {
      this.vendorOptions = res;
      this.dataShareService.changeVendors(res);
    });
    this.getBrands().subscribe((res) => {
      this.brandOptions = res;
      this.dataShareService.changeBrands(res);
    });
    this.getCategories().subscribe((res) => {
      this.catOptions = res;
      this.dataShareService.changeCategories(res);
    });
  }

  _filter(value: any, autoCompType: string): any[] {
    if (value == null) {
      return;
    }
    let name = '';
    if (value.name) {
      name = value.name;
    } else {
      name = value;
    }
    let options: any;
    switch (autoCompType) {
      case 'vendor':
        options = this.vendorOptions;
        break;
      case 'brand':
        options = this.brandOptions;
        break;
      case 'cat':
        options = this.catOptions;
    }
    if (name == null) {
      return options;
    }
    return options.filter((option) => option.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  saveProduct(product: Product): Observable<any> {
    return this.http.post('inventory/product', product);
  }

  saveVendor(vendor: Vendor): Observable<any> {
    return this.http.post('inventory/vendor', vendor);
  }

  saveBrand(brand: Brand): Observable<any> {
    return this.http.post('inventory/brand', brand);
  }

  saveCategory(cat: Category): Observable<any> {
    return this.http.post('inventory/category', cat);
  }

  reFetchVendor() {
    this.getVendors().subscribe((vendors) => {
      this.vendorOptions = vendors;
      this.dataShareService.changeVendors(vendors);
    });
  }
  reFetchBrand() {
    this.getBrands().subscribe((brands) => {
      this.brandOptions = brands;
      this.dataShareService.changeBrands(brands);
    });
  }
  reFetchCategories() {
    this.getCategories().subscribe((categories) => {
      this.catOptions = categories;
      this.catOptions.sort();
      this.dataShareService.changeCategories(categories);
    });
  }
  savePO(po: PurchaseOrder): Observable<any> {
    return this.http.post('inventory/purchaseOrder', po);
  }
  deletePO(po: PurchaseOrder): Observable<any> {
    return this.http.delete('inventory/purchaseOrder/' + po._id);
  }
  getPOByDateRange(timeRange: any): Observable<any> {
    return this.http.post('search/purchaseOrder/byDateRange', timeRange);
  }
  getVendors(): Observable<Vendor[]> {
    return this.http.get<Vendor[]>('search/vendors');
  }
  getBrands(): Observable<Vendor[]> {
    return this.http.get<Vendor[]>('search/brands');
  }
  getCategories(): Observable<Vendor[]> {
    return this.http.get<Vendor[]>('search/categories');
  }
  updateVendor(vendor: Vendor): Observable<any> {
    return this.http.put('inventory/vendor', vendor);
  }
  deleteVendor(vendor: Vendor): Observable<any> {
    return this.http.delete('inventory/vendor/' + vendor._id);
  }
  updateBrand(brand: Brand): Observable<any> {
    return this.http.put('inventory/brand', brand);
  }
  updateCategory(category: Category): Observable<any> {
    return this.http.put('inventory/category', category);
  }
  updateProduct(product: Product): Observable<any> {
    return this.http.put('inventory/product', product);
  }
}
