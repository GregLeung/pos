import { PurchaseOrder } from 'app/models/PurchaseOrder';
import { Category } from 'app/models/Category';
import { Brand } from 'app/models/Brand';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Item } from 'app/models/Item';

import { FormBuilder } from '@angular/forms';
import { Vendor } from 'app/models/Vendor';

@Injectable({
  providedIn: 'root',
})
export class DataShareService {
  constructor(private fb: FormBuilder) {}

  itemsSource = new BehaviorSubject<Item[]>([]);
  currentItems = this.itemsSource.asObservable();

  payableSource = new BehaviorSubject<number>(0);
  currentPayable = this.payableSource.asObservable();

  vendorsSource = new BehaviorSubject<Vendor[]>([]);
  currentVendors = this.vendorsSource.asObservable();

  brandsSource = new BehaviorSubject<Brand[]>([]);
  currentBrands = this.brandsSource.asObservable();

  categoriesSource = new BehaviorSubject<Category[]>([]);
  currentCategories = this.categoriesSource.asObservable();

  purchaseOrderSource = new BehaviorSubject<PurchaseOrder[]>([]);
  currentPurchaseOrders = this.purchaseOrderSource.asObservable();

  productUpdatedSource = new BehaviorSubject<any>({});
  currentProductUpdated = this.productUpdatedSource.asObservable();

  orderConvertedSource = new BehaviorSubject<any>({});
  orderConverted = this.orderConvertedSource.asObservable();

  changeItems(items: Item[]) {
    this.itemsSource.next(items);
  }
  changePayable(payable: number) {
    this.payableSource.next(payable);
  }
  changeVendors(vendors: Vendor[]) {
    this.vendorsSource.next(vendors);
  }
  changeBrands(brands: Brand[]) {
    this.brandsSource.next(brands);
  }
  changeCategories(categories: Category[]) {
    this.categoriesSource.next(categories);
  }
  changePurchaseOrders(pos: PurchaseOrder[]) {
    this.purchaseOrderSource.next(pos);
  }
  changeProductUpdated(product) {
    this.productUpdatedSource.next(product);
  }
  changeOrderConverted(order) {
    this.orderConvertedSource.next(order);
  }
}
