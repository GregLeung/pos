import { Injectable } from '@angular/core';
import { Product } from 'app/models/Product';
import { Item } from 'app/models/Item';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { SalesRecord } from 'app/models/SalesRecord';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SalesRecordPrintDialogComponent } from 'app/components/dialogs/sales-record-print-dialog/sales-record-print-dialog.component';
import { environment } from '../../environments/environment'; 

@Injectable({
  providedIn: 'root',
})
export class SalesService {
  itemsSource = new BehaviorSubject<Item[]>([]);
  currentItems = this.itemsSource.asObservable();

  constructor(private http: HttpClient, private dialog: MatDialog) {}

  changeItems(items: Item[]) {
    this.itemsSource.next(items);
  }

  getProduct(barcode: string): Observable<Product> {
    barcode = encodeURIComponent(barcode);
    return this.http.get<Product>('search/product/' + barcode);
  }

  getProducts(searchCriteria: any): Observable<Product[]> {
    return this.http.post<Product[]>('search/products', searchCriteria);
  }

  getProductListCsv(searchCriteria: any) {
    console.log(searchCriteria);
    let url = `${environment.baseUrl}search/products/getCsv?`;
    if(searchCriteria.category._id){
      url += `category=${searchCriteria.category._id}&`;
    }
    if(searchCriteria.brand._id){
      url += `brand=${searchCriteria.brand._id}&`;
    }
    if(searchCriteria.description){
      url += `description=${searchCriteria.description}`;
    }
    if(url.slice(-1) == '&'){
      url = url.slice(0, -1); 
    }
    window.open(url);
  }

  createSalesRecord(salesRecord: SalesRecord): Observable<any> {
    return this.http.post('salesRecord', salesRecord);
  }

  updateSalesRecord(salesRecord: SalesRecord): Observable<any> {
    return this.http.put('salesRecord', salesRecord);
  }

  searchSalesRecordsByCriterias(request): Observable<any> {
    return this.http.post('search/salesRecords/byCriterias', request);
  }

  convertToInvoice(salesRecord: SalesRecord): Observable<any> {
    return this.http.post('salesRecord/convert', salesRecord);
  }

  openPrintDialog(response): Promise<any> {
    var self = this;
    return new Promise(function (resolve, reject) {
      const dialogConfig = new MatDialogConfig();
      // dialogConfig.minHeight = '100%';
      dialogConfig.minWidth = '80%';
      const salesRecord = response;
      const data = { salesRecord: salesRecord };
      dialogConfig.data = data;
      const dialogRef = self.dialog.open(SalesRecordPrintDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((value) => {
        resolve(value);
      });
    });
  }
}
