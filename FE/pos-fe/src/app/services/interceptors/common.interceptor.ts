import { SnackBarService } from 'app/services/snack-bar.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class CommonInterceptor implements HttpInterceptor {
  constructor(private snackBarService: SnackBarService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const baseUrl = environment.baseUrl;
    const req = request.clone({
      url: baseUrl.concat(request.url),
    });
    return next.handle(req).pipe(
      catchError(async (e) => this.snackBarService.openSnackBar('Error', e)),
      tap((data: HttpResponse<any>) => {
        if (data !== undefined && (data.status === 201 || data.status === 204)) {
          this.snackBarService.openSnackBar('Success', data);
        }
      })
    );
  }
}
