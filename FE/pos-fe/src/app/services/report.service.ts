import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment'; 

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpClient) {}

  getSalesReportByDateRange(dateRange): Observable<any> {
    return this.http.post('search/report/byTimeRange', dateRange);
  }

  getCsvFile(startDate, endDate){
    let query = 'startDate=' + startDate + '&endDate=' + endDate;
    window.open(`${environment.baseUrl}search/report/getCsv?${query}`);
  }

}
