export class DisplayHelper {
  static displayName(obj): string {
    if (!obj) {
      return '';
    }
    return obj.name;
  }
}
