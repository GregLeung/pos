import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';


import { CommonInterceptor } from './services/interceptors/common.interceptor';

import { PaymentMethodListComponent } from './components/sales/payment-method-list/payment-method-list.component';
import { OrderComponent } from './components/inventory/entry/order/order.component';
import { VendorComponent } from './components/inventory/entry/vendor/vendor.component';
import { BrandComponent } from './components/inventory/entry/brand/brand.component';
import { CategoryComponent } from './components/inventory/entry/category/category.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SalesComponent } from './components/sales/sales.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { ReportComponent } from './components/report/report.component';
import { ItemTableComponent } from './components/shared/item-table/item-table.component';
import { ProductComponent } from './components/inventory/entry/product/product.component';
import { SerialNumberDialogComponent } from './components/shared/serial-number-dialog/serial-number-dialog.component';
import { ManagementComponent } from './components/management/management.component';
import { InventoryMgtComponent } from './components/management/inventory/inventory-mgt/inventory-mgt.component';
import { ProductMgtComponent } from './components/management/inventory/product-mgt/product-mgt.component';
import { PurchaseOrderMgtComponent } from './components/management/inventory/purchase-order-mgt/purchase-order-mgt.component';
import { BrandMgtComponent } from './components/management/inventory/brand-mgt/brand-mgt.component';
import { CategoryMgtComponent } from './components/management/inventory/category-mgt/category-mgt.component';
import { VendorMgtComponent } from './components/management/inventory/vendor-mgt/vendor-mgt.component';
import { AdvancedProductSearchDialogComponent } from './components/shared/advanced-product-search-dialog/advanced-product-search-dialog.component';
import { GetCostByIndexPipe } from './pipes/get-cost-by-index.pipe';
import { BrandFormComponent } from './components/shared/forms/brand-form/brand-form.component';
import { VendorFormComponent } from './components/shared/forms/vendor-form/vendor-form.component';
import { ProductFormComponent } from './components/shared/forms/product-form/product-form.component';
import { CategoryFormComponent } from './components/shared/forms/category-form/category-form.component';
import { PurchaseOrderFormComponent } from './components/shared/forms/purchase-order-form/purchase-order-form.component';
import { SalesRecordComponent } from './components/sales/sales-record/sales-record.component';
import { SalesRecordMgtComponent } from './components/management/salesRecord/sales-record-mgt/sales-record-mgt.component';
import { PaymentFormComponent } from './components/shared/forms/payment-form/payment-form.component';
import { DynamicFormControlComponent } from './components/shared/forms/dynamic-form-control/dynamic-form-control.component';
import { ConfirmationComponent } from './components/sales/confirmation/confirmation.component';
import { TimeRangeFormComponent } from './components/shared/forms/time-range-form/time-range-form.component';
import { SalesRecordActionDialogComponent } from './components/shared/sales-record-action-dialog/sales-record-action-dialog.component';
import { ConfirmationDialogComponent } from './components/shared/confirmation-dialog/confirmation-dialog.component';
import { SalesRecordPrintDialogComponent } from './components/dialogs/sales-record-print-dialog/sales-record-print-dialog.component';
import { HeaderComponent } from './components/shared/printing/header/header.component';
import { FooterComponent } from './components/shared/printing/footer/footer.component';
import { AutoFocusDirective } from './components/shared/directives/auto-focus.directive';
import { PasswordDialogComponent } from './components/shared/password-dialog/password-dialog.component';

export const DateFormats = {
  parse: {
    dateInput: ['DD/MM/YYYY'],
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SalesComponent,
    InventoryComponent,
    ReportComponent,
    PaymentMethodListComponent,
    InventoryComponent,
    OrderComponent,
    VendorComponent,
    BrandComponent,
    CategoryComponent,
    ItemTableComponent,
    ProductComponent,
    SerialNumberDialogComponent,
    ManagementComponent,
    InventoryMgtComponent,
    ProductMgtComponent,
    PurchaseOrderMgtComponent,
    BrandMgtComponent,
    CategoryMgtComponent,
    VendorMgtComponent,
    AdvancedProductSearchDialogComponent,
    GetCostByIndexPipe,
    BrandFormComponent,
    VendorFormComponent,
    ProductFormComponent,
    CategoryFormComponent,
    PurchaseOrderFormComponent,
    SalesRecordComponent,
    SalesRecordMgtComponent,
    PaymentFormComponent,
    DynamicFormControlComponent,
    ConfirmationComponent,
    TimeRangeFormComponent,
    SalesRecordActionDialogComponent,
    ConfirmationDialogComponent,
    SalesRecordPrintDialogComponent,
    HeaderComponent,
    FooterComponent,
    AutoFocusDirective,
    PasswordDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    FlexLayoutModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatListModule,
    MatSelectModule,
    MatExpansionModule,
    MatDividerModule,
    MatSnackBarModule,
    MatToolbarModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CommonInterceptor, multi: true },
    MatDatepickerModule,
    MatMomentDateModule,
    { provide: MAT_DATE_FORMATS, useValue: DateFormats },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
