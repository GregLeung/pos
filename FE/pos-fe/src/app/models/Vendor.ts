export interface Vendor {
  _id?: string;
  name: string;
  phoneNumbers: number[];
  faxNumbers: number[];
  addresses: string[];
  emails: string[];
  paymentPeriodDays: number;
}
