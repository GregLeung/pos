import { Product } from 'app/models/Product';

export class Item {
  _id?: string;
  barcode: string;
  name?: string;
  desc?: string;
  originalQty: number;
  qty: number;
  price: number;
  adjustedPrice?: number;
  snExists?: boolean;
  originalSN?: string[];
  sn?: string[];
  cost?: number;
  constructor(product: Product) {
    this._id = product._id;
    this.barcode = product.barcode;
    this.name = product.name;
    this.desc = product.desc;
    this.price = product.price;
    this.adjustedPrice = product.price;
    this.qty = product.qty;
    this.originalSN = product.sn;
    this.sn = [];
    this.cost = product.cost;
    this.originalQty = product.qty;
  }
}
