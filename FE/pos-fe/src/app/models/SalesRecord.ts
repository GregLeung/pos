import { PaymentMethod } from 'app/models/PaymentMethod';
import { Item } from 'app/models/Item';

export class SalesRecord {
  _id?: string;
  recordType: string;
  total: number;
  salesman: string;
  fullFilled?: boolean;
  convertedTo?: string;
  convertedFrom?: string;
  payments: PaymentMethod[];
  items: Item[];
  isDeleted?: boolean;
  constructor(type: string, payments: PaymentMethod[], items: Item[], otherInfo: any) {
    this.recordType = type;
    this.payments = payments;
    this.items = items;
    this.salesman = otherInfo.salesman;
    switch (type) {
      case 'invoice':
        this.total = otherInfo.payable;
        return;
      case 'order':
        if (otherInfo.payment >= otherInfo.payable) {
          this.total = otherInfo.payable;
        } else {
          this.total = otherInfo.payment;
        }
        return;
      default:
        this.total = otherInfo.payment;
        return;
    }
  }
}
