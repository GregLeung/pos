import { Item } from 'app/models/Item';
export class PurchaseOrder {
  _id?: string;
  vendor: string;
  vendorRefNum: number;
  vendorOrderDate: Date;
  purchaser: string;
  remark: string;
  items: [Item];
  constructor(vendorInfoForm: any) {
    this.vendor = vendorInfoForm.vendor;
    this.vendorRefNum = vendorInfoForm.vendorRefNum;
    this.vendorOrderDate = vendorInfoForm.vendorOrderDate;
    this.purchaser = vendorInfoForm.purchaser;
    this.remark = vendorInfoForm.remark;
    this.items = vendorInfoForm.items;
  }
}
