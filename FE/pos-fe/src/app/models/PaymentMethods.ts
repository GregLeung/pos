import { PaymentMethod } from 'app/models/PaymentMethod';

export const PAYMENT_METHODS: PaymentMethod[] = [
  { name: 'CASH', rate: 0, amt: null },
  { name: 'EPS', rate: 0.01, amt: null },
  { name: 'VISA', rate: 0.01, amt: null },
  { name: 'MASTER', rate: 0.02, amt: null },
  { name: 'UNION PAY', rate: 0, amt: null },
  { name: 'AE', rate: 0.02, amt: null },
  { name: 'P. Card', rate: 0, amt: null },
  { name: 'CR Note', rate: 0, amt: null },
  { name: 'Bank Tsf.', rate: 0, amt: null },
];
