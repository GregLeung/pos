export const ACTIONS = {
  EDIT: { action: 'edit', btnDisplay: 'UPDATE' },
  CREATE: { action: 'create', btnDisplay: 'SUBMIT' },
  CONVERT: { action: 'convert', btnDisplay: 'CONVERT' },
};
