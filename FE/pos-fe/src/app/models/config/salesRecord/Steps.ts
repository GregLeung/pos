export class Step {
  prev: Step | null;
  next: Step | null;
  pageNumber: number;
  isFinalStep: boolean;
  constructor(pageNumber) {
    this.pageNumber = pageNumber;
  }
}

export class InvoiceSteps {
  root: Step;

  constructor() {
    this.root = new Step(0);
    const step2 = new Step(1);
    const step3 = new Step(2);
    step2.next = step3;
    step2.prev = this.root;
    step3.next = null;
    step3.prev = step2;
    step3.isFinalStep = true;
    this.root.next = step2;
    this.root.prev = null;
  }
}

export class OrderSteps {
  root: Step;

  constructor() {
    this.root = new Step(0);
    const step2 = new Step(1);
    const step3 = new Step(2);
    step2.next = step3;
    step2.prev = this.root;
    step3.next = null;
    step3.prev = step2;
    step3.isFinalStep = true;
    this.root.next = step2;
    this.root.prev = null;
  }
}

export class QuotationSteps {
  root: Step;

  constructor() {
    this.root = new Step(0);
    const step2 = new Step(2);
    step2.next = null;
    step2.prev = this.root;
    step2.isFinalStep = true;
    this.root.next = step2;
    this.root.prev = null;
  }
}

export class ReturnSteps {
  root: Step;

  constructor() {
    this.root = new Step(0);
    const step2 = new Step(1);
    const step3 = new Step(2);
    step2.next = step3;
    step2.prev = this.root;
    step3.next = null;
    step3.prev = step2;
    step3.isFinalStep = true;
    this.root.next = step2;
    this.root.prev = null;
  }
}
