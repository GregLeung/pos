import { FormGroup } from '@angular/forms';

// export class InvoiceDetails implements Details {
export class InvoiceDetails {
  columns: { label: string; value: any }[];

  constructor(paymentForm: FormGroup) {
    this.columns = new Array({ label: 'Total Amount', value: paymentForm.get('payable').value });
    this.columns.push({ label: 'Total Payment', value: paymentForm.get('payment').value });
    this.columns.push({ label: 'Change', value: paymentForm.get('change').value });
  }
}

export class ReturnDetails {
  columns: { label: string; value: any }[];

  constructor(paymentForm: FormGroup) {
    this.columns = new Array({ label: 'Total Amount', value: paymentForm.get('payable').value });
    this.columns.push({ label: 'Credit Note', value: paymentForm.get('payment').value });
  }
}

export class QuotationDetails {
  columns: { label: string; value: any }[];

  constructor(paymentForm: FormGroup) {
    this.columns = new Array({ label: 'Total Amount', value: paymentForm.get('payable').value });
  }
}

export class OrderDetails {
  columns: { label: string; value: any }[];

  constructor(paymentForm: FormGroup) {
    this.columns = new Array({ label: 'Total Amount', value: paymentForm.get('payable').value });
    this.columns.push({ label: 'Total Deposit', value: paymentForm.get('payment').value });
  }
}
