export const SALES_RECORD_TYPE = {
  INVOICE: 'invoice',
  QUOTATION: 'quotation',
  ORDER: 'order',
  RETURN: 'return',
};
