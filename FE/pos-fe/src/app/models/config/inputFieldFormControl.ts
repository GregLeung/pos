import { FormControlBase } from 'app/models/config/formControlBase';

export class InputFieldFormControl extends FormControlBase<number> {
  controlType = 'input';
}
