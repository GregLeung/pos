export interface Product {
  _id?: string;
  barcode: string;
  productCode?: string[];
  name?: string;
  desc?: string;
  qty: number;
  price: number;
  cost?: number;
  sn?: string[];
  brand?: string;
  category?: string;
  newIds?: string[];
  idToRemove?: string[];
}
