import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getCostByIndex',
})
export class GetCostByIndexPipe implements PipeTransform {
  transform(value: any, index: number): string {
    if (typeof value === 'number') {
      value = value.toString();
    }
    value = value.padStart(4, '0');
    const target = value.charAt(index);
    return target === '0' ? '' : target;
  }
}
