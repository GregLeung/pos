import { DataShareService } from 'app/services/data-share.service';
import { SnackBarService } from 'app/services/snack-bar.service';
import {
  OrderDetails,
  QuotationDetails,
  InvoiceDetails,
  ReturnDetails,
} from 'app/models/config/salesRecord/ConfDetails';
import { InvoiceSteps, ReturnSteps, OrderSteps, QuotationSteps, Step } from 'app/models/config/salesRecord/Steps';
import {
  Component,
  Input,
  OnInit,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
  ViewChild,
  HostListener,
  AfterViewInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { SalesService } from 'app/services/sales.service';
import { Item } from 'app/models/Item';
import { SalesRecord } from 'app/models/SalesRecord';
import { PaymentMethod } from 'app/models/PaymentMethod';

import { MatDialog } from '@angular/material/dialog';

import { ACTIONS } from 'app/models/config/salesRecord/Actions';

import { ItemTableComponent } from 'app/components/shared/item-table/item-table.component';

@Component({
  selector: 'app-sales-record',
  templateUrl: './sales-record.component.html',
  styleUrls: ['./sales-record.component.css'],
})
export class SalesRecordComponent implements OnInit, OnChanges, AfterViewInit {
  constructor(
    private dataShareService: DataShareService,
    private salesService: SalesService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private snakeBar: SnackBarService
  ) {}

  @Input() salesRecordType: string;
  @Input() salesRecord: SalesRecord;
  @Input() action = ACTIONS.CREATE;

  disableHotKeys = false;

  @ViewChild(ItemTableComponent) private itemTableComponent: ItemTableComponent;

  payable: number;

  items: Item[] = [];
  paymentMethods: PaymentMethod[] = [
    { name: 'CASH', amt: null },
    { name: 'EPS', amt: null },
    { name: 'VISA', amt: null },
    { name: 'MASTER', amt: null },
    { name: 'UNION PAY', amt: null },
    { name: 'AE', amt: null },
    { name: 'P. Card', amt: null },
    { name: 'CR Note', amt: null },
    { name: 'Bank Tsf.', amt: null },
  ];
  pageNumber: number;
  paymentForm = this.fb.group({
    payable: [0],
    payment: [0],
    change: [{ value: 1 }, Validators.min(0)],
    salesman: [''],
    paymentMethods: [],
  });

  details: any;
  step: Step;

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (!event.ctrlKey || this.disableHotKeys) {
      return;
    }
    switch (event.key) {
      case 'ArrowRight':
        this.advance();
        break;
      case 'ArrowLeft':
        this.goBack();
        break;
      case 'F3':
        this.itemTableComponent.onAdvancedSearch();
    }
  }
  ngOnInit(): void {
    this.pageNumber = 0;
    this.payable = 0;
  }
  ngAfterViewInit(): void {
    this.itemTableComponent.focusBarcodeInputField();
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'salesRecord': {
            this.items = this.salesRecord.items;
            this.paymentMethods = this.salesRecord.payments;
            this.paymentForm.get('salesman').setValue(this.salesRecord.salesman);
            break;
          }
          case 'salesRecordType': {
            this.setRootStep();
            break;
          }
        }
      }
    }
  }

  setRootStep(): void {
    switch (this.salesRecordType) {
      case 'invoice':
        this.step = new InvoiceSteps().root;
        break;
      case 'order':
        this.paymentForm.get('change').clearValidators();
        this.paymentForm.get('payment').setValidators([Validators.min(1)]);
        this.step = new OrderSteps().root;
        break;
      case 'quotation':
        this.paymentForm.get('change').clearValidators();
        this.step = new QuotationSteps().root;
        break;
      case 'return':
        this.step = new ReturnSteps().root;
        this.paymentForm.get('change').clearValidators();
        this.paymentForm.get('change').setValidators([Validators.max(0), Validators.min(0)]);
        break;
    }
  }

  updateItems($event): void {
    this.items = $event;
    this.updatePayable();
  }

  updatePayable(): void {
    let totalAmt = 0;
    const iterator = this.items.values();
    for (const i of iterator) {
      totalAmt += i.adjustedPrice * i.qty;
    }
    this.payable = totalAmt;
    this.paymentForm.get('payable').setValue(this.payable);
    this.paymentForm.get('change').setValue(this.payable * -1);
    this.calculatePayment();
  }

  updatePaymentMethods($event): void {
    this.paymentMethods = $event;
    this.calculatePayment();
  }

  updatePaymentFormValue(payable: number, paymentAmt: number, changeAmt: number): void {
    this.paymentForm.patchValue({
      payable: payable,
      payment: paymentAmt,
      change: changeAmt,
    });
  }

  updateConfDetails(): void {
    switch (this.salesRecordType) {
      case 'invoice':
        this.details = new InvoiceDetails(this.paymentForm);
        break;
      case 'return':
        this.paymentForm.get('payment').setValue(this.payable);
        this.details = new ReturnDetails(this.paymentForm);
        break;
      case 'quotation':
        this.details = new QuotationDetails(this.paymentForm);
        break;
      case 'order':
        this.details = new OrderDetails(this.paymentForm);
    }
  }

  calculatePayment(): void {
    if (this.paymentForm === undefined) {
      return;
    }
    let paymentAmt = 0;
    for (const ele of this.paymentMethods.values()) {
      if (ele.amt != null) {
        ele.amt = Math.round(ele.amt);
        paymentAmt += ele.amt;
      }
    }
    const changeAmt = paymentAmt - this.payable;
    this.updatePaymentFormValue(this.payable, paymentAmt, changeAmt);
  }

  validate(nextStep): boolean {
    const nextPageNumber = nextStep ? nextStep.pageNumber : -1;

    switch (nextPageNumber) {
      case 1:
        return this.isItemListValid();
      case 2:
        this.paymentForm.get('change').markAsTouched();
        return this.paymentForm.valid;
      default:
        this.submit();
    }
  }

  advance(): void {
    const next = this.step.next;
    if (!this.validate(next)) {
      return;
    }

    this.pageNumber = next ? next.pageNumber : this.pageNumber;
    this.step = next ? next : this.step;
    switch (this.pageNumber) {
      case 1:
        this.updatePayable();
        break;
      case 2:
        this.paymentForm.get('salesman').setValidators([Validators.required]);
        this.updateConfDetails();
        this.cdr.detectChanges();
    }
  }

  reset(): void {
    this.items = [];
  }

  goBack(): void {
    const prev = this.step.prev;
    this.pageNumber = prev ? prev.pageNumber : this.pageNumber;
    this.step = prev ? prev : this.step;
    this.paymentForm.get('salesman').clearValidators();
    this.paymentForm.get('salesman').updateValueAndValidity();
  }

  adjustPayment(): void {
    this.paymentMethods.forEach((payment) => {
      payment.amt = payment.amt === undefined ? null : payment.amt;
    });
  }

  submit(): void {
    this.paymentForm.markAsTouched();
    if (!this.paymentForm.valid) {
      return;
    }
    const paymentMethods = this.paymentMethods;
    this.adjustPayment();
    const salesRecord: SalesRecord = new SalesRecord(
      this.salesRecordType,
      paymentMethods,
      this.items,
      this.paymentForm.value
    );
    switch (this.action.action) {
      case 'edit':
        salesRecord._id = this.salesRecord._id;
        this.salesService.updateSalesRecord(salesRecord).subscribe((res) => {});
        break;
      case 'create':
        this.salesService.createSalesRecord(salesRecord).subscribe((res) => {
          this.openPrintDialog(res);
        });
        break;
      case 'convert':
        salesRecord.convertedFrom = this.salesRecord._id;
        this.salesService.convertToInvoice(salesRecord).subscribe((res) => {
          this.dataShareService.changeOrderConverted(res);
          this.dataShareService.changeOrderConverted({});
          this.openPrintDialog(res);
        });
    }
  }

  openPrintDialog(response): void {
    this.disableHotKeys = true;
    this.salesService.openPrintDialog(response).then((value) => {
      this.onReset();
      this.disableHotKeys = false;
    });
  }

  onReset() {
    this.pageNumber = 0;
    this.payable = 0;
    this.itemTableComponent.resetItemTable();
    this.itemTableComponent.focusBarcodeInputField();
    this.items = [];
    this.paymentForm.get('salesman').clearValidators();
    this.paymentForm.reset();
    this.setRootStep();
    this.paymentMethods.forEach((ele) => (ele.amt = null));
  }

  isItemListValid(): boolean {
    let msg = '';
    let result = true;
    if (this.items.length === 0) {
      this.snakeBar.showValidationMsg(`Scan an item to start With.`);
      return false;
    }
    this.items.forEach((ele) => {
      if (ele.adjustedPrice < ele.cost && this.salesRecordType === 'invoice') {
        msg += `\n${ele.barcode} is under cost.`;
        result = false;
      }
      if (ele.qty > ele.originalQty && this.salesRecordType === 'invoice') {
        msg += `\n${ele.barcode} qty is larger than current stock.`;
        result = false;
      }
      if (ele.qty <= 0) {
        msg += `\n${ele.barcode} Qty is invalid.`;
        result = false;
      }
    });
    if (!result) {
      this.snakeBar.showValidationMsg(msg.trim());
    }
    return result;
  }
}
