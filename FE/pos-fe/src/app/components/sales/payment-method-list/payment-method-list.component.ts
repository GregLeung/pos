import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  QueryList,
  ViewChildren,
  AfterViewInit,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PaymentMethod } from 'app/models/PaymentMethod';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-payment-method-list',
  templateUrl: './payment-method-list.component.html',
  styleUrls: ['./payment-method-list.component.css'],
})
export class PaymentMethodListComponent implements OnInit, OnChanges, AfterViewInit {
  @Output() parentSubmit = new EventEmitter<any>();
  @Output() updateParentPaymentMethods = new EventEmitter<any>();
  @Input() payable: number;
  @Input() paymentForm;
  @Input() paymentMethodsInput;
  @Input() readOnly = false;
  @Input() currPageNumber: number;

  displayedColumns: string[] = ['title', 'amt'];
  displayedColumnsFooter: string[] = ['title', 'amt'];

  dataSource = new MatTableDataSource<PaymentMethod>();

  @ViewChildren(MatInput) inputs!: QueryList<MatInput>;

  paymentMethods: PaymentMethod[] = [
    { name: 'CASH', amt: null },
    { name: 'EPS', amt: null },
    { name: 'VISA', amt: null },
    { name: 'MASTER', amt: null },
    { name: 'UNION PAY', amt: null },
    { name: 'AE', amt: null },
    { name: 'P. Card', amt: null },
    { name: 'CR Note', amt: null },
    { name: 'Bank Tsf.', amt: null },
  ];

  constructor() {}

  ngOnInit(): void {
    this.dataSource.data = this.paymentMethods;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'paymentMethodsInput': {
            if (this.paymentMethodsInput === undefined) {
              return;
            }
            this.paymentMethodsInput.forEach((inputPaymentMethod) => {
              if (inputPaymentMethod.amt !== undefined) {
                const index = this.paymentMethods.findIndex((element) => element.name === inputPaymentMethod.name);
                const temp = this.paymentMethods[index];
                temp.amt = inputPaymentMethod.amt;
                this.paymentMethods[index] = temp;
              }
            });
            this.updateParentPaymentMethods.next(this.paymentMethods);
            this.dataSource.data = this.paymentMethods;
          }
          case 'currPageNumber': {
            if (this.inputs) {
              this.inputs.first.focus();
            }
          }
        }
      }
    }
  }

  ngAfterViewInit() {}

  updatePayMethods(): void {
    this.updateParentPaymentMethods.next(this.dataSource.data);
  }
}
