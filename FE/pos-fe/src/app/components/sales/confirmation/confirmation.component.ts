import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css'],
})
export class ConfirmationComponent implements OnInit, OnChanges {
  @Input() itemsInput;
  @Input() paymentMethods;
  @Input() details;
  @Input() paymentForm;
  filteredSalesmanOptions: Observable<any[]>;
  salesmanOptions: any[] = [{ name: 'Cow' }, { name: 'Bird' }, { name: 'Snake' }];

  constructor() {}

  get salesman() {
    return this.paymentForm.get('salesman');
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'paymentMethods': {
            if (this.paymentMethods === undefined) {
              return;
            }
            this.paymentMethods = this.paymentMethods.filter((ele) => ele.amt != null);
            break;
          }
          case 'paymentForm': {
            this.filteredSalesmanOptions = this.paymentForm.get('salesman').valueChanges.pipe(
              startWith(''),
              map((value) => this._filter(value))
            );
          }
        }
      }
    }
  }
  private _filter(name): any[] {
    return name
      ? this.salesmanOptions.filter((option) => option.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
      : this.salesmanOptions;
  }
}
