import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';

import { SALES_RECORD_TYPE } from 'app/models/config/salesRecordType';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css'],
})
export class SalesComponent implements OnInit {
  constructor(private cdr: ChangeDetectorRef) {}

  TOTAL_PAGE: number;
  SALES_RECORD_TYPES: any;
  selectedIndex: number;
  ngOnInit(): void {
    this.SALES_RECORD_TYPES = SALES_RECORD_TYPE;
    this.selectedIndex = 0;
    this.TOTAL_PAGE = 4;
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (!event.ctrlKey) {
      return;
    }
    switch (event.code) {
      case 'KeyZ':
        this.next();
        break;
    }
  }

  next():void{
    let temp = this.selectedIndex;
    temp++;
    this.selectedIndex = temp % this.TOTAL_PAGE;    
  }
  
}
