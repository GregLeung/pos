import { Component, OnInit } from '@angular/core';
import { INVENTORY_TYPES } from 'app/models/config/InventoryTypeList';

@Component({
  selector: 'app-inventory-mgt',
  templateUrl: './inventory-mgt.component.html',
  styleUrls: ['./inventory-mgt.component.css'],
})
export class InventoryMgtComponent implements OnInit {
  constructor() {}

  selectedInvType: string;
  INVENTORY_TYPES = INVENTORY_TYPES;

  ngOnInit(): void {}
}
