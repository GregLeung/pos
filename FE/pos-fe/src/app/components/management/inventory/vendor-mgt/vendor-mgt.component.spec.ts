import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMgtComponent } from './vendor-mgt.component';

describe('VendorMgtComponent', () => {
  let component: VendorMgtComponent;
  let fixture: ComponentFixture<VendorMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
