import { Vendor } from 'app/models/Vendor';
import { Component, OnInit } from '@angular/core';
import { DataShareService } from 'app/services/data-share.service';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-vendor-mgt',
  templateUrl: './vendor-mgt.component.html',
  styleUrls: ['./vendor-mgt.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class VendorMgtComponent implements OnInit {
  dataSource = [];
  columnsToDisplay = ['name', 'phoneNumbers', 'faxNumbers', 'addresses', 'paymentPeriodDays'];
  expandedElement: Vendor | null;

  vendors: Vendor[] = [];

  constructor(private dataShareService: DataShareService) {}
  ngOnInit(): void {
    this.dataShareService.vendorsSource.subscribe(vendors => {
      this.vendors = vendors;
      this.dataSource = this.vendors;
    });
  }
  onEdit(index: number): void {}
}
