import { AdvancedProductSearchDialogComponent } from 'app/components/shared/advanced-product-search-dialog/advanced-product-search-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DataShareService } from 'app/services/data-share.service';
import { SalesService } from 'app/services/sales.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'app/models/Product';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-product-mgt',
  templateUrl: './product-mgt.component.html',
  styleUrls: ['./product-mgt.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProductMgtComponent implements OnInit {
  // dataSource = [];
  dataSource = new MatTableDataSource<Product>();

  columnsToDisplay: string[] = ['barcode', 'category', 'brand', 'qty', 'cost', 'price'];

  barcodeToSearch: string;
  products: Product[] = [];

  expandedElement: Product | null;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private dataShareService: DataShareService
  ) {}

  ngOnInit(): void {
    this.dataShareService.productUpdatedSource.subscribe((product) => {
      if (Object.keys(product).length > 0) {
        console.log(product);
        const updatedProductIndex = this.products.findIndex((ele) => ele._id === product._id);
        this.products[updatedProductIndex] = product;
        this.dataSource.data = this.products;
      }
    });
  }

  onSearch(): void {
    this.barcodeToSearch = this.barcodeToSearch.toUpperCase();
    this.salesService.getProduct(this.barcodeToSearch).subscribe((product) => {
      this.addProduct(product);
      this.barcodeToSearch = '';
    });
  }

  addProduct(newProduct: Product): void {
    let productExists = false;
    this.products.forEach((product) => {
      if (product.barcode === newProduct.barcode) {
        productExists = !productExists;
      }
    });

    if (!productExists) {
      this.products.push(newProduct);
      this.dataSource.data = this.products;
    }
  }

  onAdvancedSearch(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '80%';
    const dialogRef = this.dialog.open(AdvancedProductSearchDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        this.products = value;
        this.dataSource.data = this.products;
      }
    });
  }

  onEdit(rowIdx: number): void {}
}
