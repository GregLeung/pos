import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { InventoryService } from 'app/services/inventory.service';
import { PurchaseOrder } from 'app/models/PurchaseOrder';
import { DataShareService } from 'app/services/data-share.service';

@Component({
  selector: 'app-purchase-order-mgt',
  templateUrl: './purchase-order-mgt.component.html',
  styleUrls: ['./purchase-order-mgt.component.css'],
})
export class PurchaseOrderMgtComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private inventoryService: InventoryService,
    private dataShareService: DataShareService
  ) {}

  purchaseOrders: PurchaseOrder[] = [];

  poQueryForm = this.fb.group({
    startDate: ['', Validators.required],
    endDate: ['', Validators.required],
  });
  queryPO($event): void {
    $event.startDate.set({ hour: 0, minute: 0, second: 0 });
    $event.endDate.set({ hour: 23, minute: 59, second: 59 });
    this.inventoryService.getPOByDateRange($event).subscribe((res) => {
      this.dataShareService.changePurchaseOrders(res.result);
    });
  }
  ngOnInit(): void {
    this.dataShareService.currentPurchaseOrders.subscribe((pos) => (this.purchaseOrders = pos));
  }
}
