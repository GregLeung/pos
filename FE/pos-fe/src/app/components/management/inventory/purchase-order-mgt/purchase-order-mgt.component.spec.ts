import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderMgtComponent } from './purchase-order-mgt.component';

describe('PurchaseOrderMgtComponent', () => {
  let component: PurchaseOrderMgtComponent;
  let fixture: ComponentFixture<PurchaseOrderMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
