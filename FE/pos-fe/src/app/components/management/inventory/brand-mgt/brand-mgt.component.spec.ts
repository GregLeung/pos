import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandMgtComponent } from './brand-mgt.component';

describe('BrandMgtComponent', () => {
  let component: BrandMgtComponent;
  let fixture: ComponentFixture<BrandMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
