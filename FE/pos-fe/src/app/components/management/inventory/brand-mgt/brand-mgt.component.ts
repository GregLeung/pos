import { DataShareService } from 'app/services/data-share.service';
import { Component, OnInit } from '@angular/core';
import { Brand } from 'app/models/Brand';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-brand-mgt',
  templateUrl: './brand-mgt.component.html',
  styleUrls: ['./brand-mgt.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BrandMgtComponent implements OnInit {
  constructor(private dataShareService: DataShareService) {}
  dataSource = [];
  columnsToDisplay = ['name'];
  expandedElement: Brand | null;

  brands: Brand[] = [];

  ngOnInit(): void {
    this.dataShareService.brandsSource.subscribe((brands) => {
      this.brands = brands;
      this.dataSource = this.brands;
    });
  }
  onEdit(index: number): void {}
}
