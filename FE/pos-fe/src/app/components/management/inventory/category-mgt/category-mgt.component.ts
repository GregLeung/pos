import { Category } from 'app/models/Category';
import { DataShareService } from 'app/services/data-share.service';
import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-category-mgt',
  templateUrl: './category-mgt.component.html',
  styleUrls: ['./category-mgt.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CategoryMgtComponent implements OnInit {
  constructor(private dataShareService: DataShareService) {}
  categories: Category[] = [];
  dataSource = [];
  columnsToDisplay = ['name'];
  expandedElement: Category | null;
  ngOnInit(): void {
    this.dataShareService.categoriesSource.subscribe((categories) => {
      this.categories = categories;
      this.dataSource = this.categories;
    });
  }
  onEdit(index: number): void {}
}
