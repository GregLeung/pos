import { Component, OnInit } from '@angular/core';
import { InventoryService } from 'app/services/inventory.service';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css'],
})
export class ManagementComponent implements OnInit {
  constructor(private inventoryService: InventoryService) {}

  ngOnInit(): void {}
}
