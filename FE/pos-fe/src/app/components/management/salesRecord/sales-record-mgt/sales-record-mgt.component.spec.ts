import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRecordMgtComponent } from './sales-record-mgt.component';

describe('SalesRecordMgtComponent', () => {
  let component: SalesRecordMgtComponent;
  let fixture: ComponentFixture<SalesRecordMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRecordMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRecordMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
