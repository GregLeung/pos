import { DataShareService } from 'app/services/data-share.service';
import { Component, OnInit } from '@angular/core';
import { SALES_RECORD_TYPES } from 'app/models/config/SalesRecordTypeList';
import { SalesRecord } from 'app/models/SalesRecord';
import { SalesService } from 'app/services/sales.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SalesRecordActionDialogComponent } from 'app/components/shared/sales-record-action-dialog/sales-record-action-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/shared/confirmation-dialog/confirmation-dialog.component';

import { ACTIONS } from 'app/models/config/salesRecord/Actions';
@Component({
  selector: 'app-sales-record-mgt',
  templateUrl: './sales-record-mgt.component.html',
  styleUrls: ['./sales-record-mgt.component.css'],
})
export class SalesRecordMgtComponent implements OnInit {
  selectedSalesRecordType: string;
  SALES_RECORD_TYPES = SALES_RECORD_TYPES;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private dataShareService: DataShareService
  ) {}

  ngOnInit(): void {
    this.dataShareService.orderConvertedSource.subscribe((converted) => {
      if (Object.keys(converted).length > 0) {
        const index = this.records.findIndex((ele) => ele._id === converted.convertedFrom);
        if (index != -1) {
          this.records[index].fullFilled = true;
          this.dataSource.data = this.records;
        }
      }
    });
  }

  records: SalesRecord[] = [];

  dataSource = new MatTableDataSource<SalesRecord>();

  displayedColumns = ['id', 'salesman', 'totalAmount', 'createdAt', 'voided', 'action'];

  searchByTimeRange($event): void {
    $event.type = this.selectedSalesRecordType.toLowerCase();
    $event.startDate.set({ hour: 0, minute: 0, second: 0 });
    $event.endDate.set({ hour: 23, minute: 59, second: 59 });
    this.salesService.searchSalesRecordsByCriterias($event).subscribe((resp) => {
      this.records = resp;
      this.dataSource.data = this.records;
    });
  }

  openEditDialog(salesRecord: SalesRecord) {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '65%';
    dialogConfig.data = { salesRecord: salesRecord, action: ACTIONS.EDIT };
    const dialogRef = this.dialog.open(SalesRecordActionDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {});
  }

  openConvertDialog(salesRecord: SalesRecord) {
    const dialogConfig = new MatDialogConfig();
    let temp  = { ...salesRecord }
    temp.recordType = 'invoice';
    // dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '70%';
    dialogConfig.minHeight = '80%';
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.data = { salesRecord: temp, action: ACTIONS.CONVERT };
    const dialogRef = this.dialog.open(SalesRecordActionDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {});
  }

  openVoidDialog(salesRecord: SalesRecord) {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '15%';
    const dialogData = { question: 'Are you sure you want to void this record?', confirmButtonDisplay: 'VOID' };
    dialogConfig.data = dialogData;
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        let temp = salesRecord;
        temp.isDeleted = true;
        this.salesService.updateSalesRecord(temp).subscribe((resp) => {
          salesRecord.isDeleted = true;
        });
      }
    });
  }

  openPrintDialog(response): void {
    this.salesService.openPrintDialog(response).then((resp) => {});
  }

  reset(): void {
    this.records = [];
    this.dataSource.data = this.records;
  }
}
