import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SalesService } from 'app/services/sales.service';
import { ReportService } from 'app/services/report.service';
import * as moment from 'moment';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
})
export class ReportComponent implements OnInit {
  startDate;
  endDate;
  report;
  constructor(private salesService: SalesService, private reportService: ReportService, private dialog: MatDialog) {}

  ngOnInit(): void {}

  searchByTimeRange($event): void {
    let startDate: moment.Moment;
    let endDate: moment.Moment;
    startDate = $event.startDate;
    endDate = $event.endDate;

    startDate = moment(startDate).subtract(1, 'day');
    startDate.set({ hour: 21, minute: 0, second: 0 });
    endDate.set({ hour: 21, minute: 0, second: 0 });
    this.startDate = startDate;
    this.endDate = endDate;
    this.reportService.getSalesReportByDateRange({ startDate, endDate }).subscribe((resp) => {
      this.report = resp;
    });
  }

  getCsvFile(): void{
    this.reportService.getCsvFile(this.startDate, this.endDate);
  }

}
