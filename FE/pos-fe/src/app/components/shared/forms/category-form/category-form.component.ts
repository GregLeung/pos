import { InventoryService } from 'app/services/inventory.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { Category } from 'app/models/Category';
import { FormBuilder, Validators, NgForm } from '@angular/forms';
import { DisplayHelper } from 'app/utils/displayHelper';
@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css'],
})
export class CategoryFormComponent implements OnInit, OnChanges {
  @Input() category: Category;

  @ViewChild('formDirective') private formDirective: NgForm;

  categoryForm = this.fb.group({
    _id: [],
    name: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private inventoryService: InventoryService) {}
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'category': {
            this.categoryForm.get('_id').setValue(this.category._id);
            this.categoryForm.get('name').setValue(this.category.name);
          }
        }
      }
    }
  }
  ngOnInit(): void {}
  onSubmit(): void {
    if (this.category) {
      this.categoryForm.get('_id').setValue(this.category._id);
      this.inventoryService.updateCategory(this.categoryForm.value).subscribe((res) => {
        this.inventoryService.reFetchCategories();
      });
    } else {
      this.inventoryService.saveCategory(this.categoryForm.value).subscribe((res) => {
        this.onReset();
        this.inventoryService.reFetchCategories();
      });
    }
  }
  displayName(obj) {
    return DisplayHelper.displayName(obj);
  }

  onReset(): void {
    this.categoryForm.reset();
    this.formDirective.resetForm();
  }
}
