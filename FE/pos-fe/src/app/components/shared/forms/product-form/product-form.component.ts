import { SerialNumberDialogComponent } from 'app/components/shared/serial-number-dialog/serial-number-dialog.component';
import { PasswordDialogComponent } from 'app/components/shared/password-dialog/password-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { InventoryService } from 'app/services/inventory.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { Product } from 'app/models/Product';
import { Brand } from 'app/models/Brand';
import { Category } from 'app/models/Category';
import { FormBuilder, NgForm, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DisplayHelper } from 'app/utils/displayHelper';
import { RequireMatch } from 'app/components/shared/forms/validators/selectFromDropdown';
import { SnackBarService } from 'app/services/snack-bar.service';
import { DataShareService } from 'app/services/data-share.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css'],
})
export class ProductFormComponent implements OnInit, OnChanges {
  @Input() product: Product;

  @ViewChild('formDirective') private formDirective: NgForm;
  @ViewChild('firstInput') private firstInput: MatInput;

  filteredBrandOptions: Observable<Brand[]>;
  filteredCategoryOptions: Observable<Category[]>;

  brandOptions: Brand[];
  catOptions: Category[];

  serialNumbers: string[] = [];

  nonEditableQty = true;

  get productCode() {
    return this.createProductForm.get('productCode') as FormArray;
  }

  createProductForm = this.fb.group({
    _id: [],
    barcode: ['', Validators.required],
    productCode: this.fb.array([this.fb.control('')]),
    name: ['', Validators.required],
    desc: ['', Validators.required],
    qty: [0],
    price: [null, Validators.required],
    cost: [null, Validators.required],
    category: [null, [Validators.required, RequireMatch]],
    brand: [null, [Validators.required, RequireMatch]],
  });

  addProductCode() {
    this.productCode.push(this.fb.control(''));
  }

  constructor(
    private fb: FormBuilder,
    private inventoryService: InventoryService,
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
    private dataShareService: DataShareService
  ) {}

  ngOnInit(): void {
    this.filteredBrandOptions = this.createProductForm.get('brand').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'brand'))
    );
    this.filteredCategoryOptions = this.createProductForm.get('category').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'cat'))
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'product': {
            this.product.productCode.forEach((ele) => {
              this.addProductCode();
            });
            this.createProductForm.patchValue(this.product);
          }
        }
      }
    }
  }

  openPasswordDialog() {
    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(PasswordDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        this.nonEditableQty = false;
      }
    });
  }

  addSerialNumberList(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '50%';
    dialogConfig.data = { count: this.createProductForm.get('qty').value, serialNumbers: this.serialNumbers };
    const dialogRef = this.dialog.open(SerialNumberDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        this.serialNumbers = value;
      }
    });
  }

  saveProduct(): void {
    if (this.createProductForm.invalid) {
      return;
    }
    const product: Product = this.createProductForm.value;
    product.sn = this.serialNumbers;
    product.barcode = product.barcode.toUpperCase().trim();
    product.productCode = product.productCode
      .filter((ele) => ele !== '' && ele !== null)
      .map((ele) => ele.toUpperCase().trim());
    if (product.productCode.includes(product.barcode)) {
      this.snackBarService.showValidationMsg(`Product Codes and Barcode: ${product.barcode} cannot be the same.`);
      return;
    }
    if (this.product) {
      product.newIds = this.getNotContained(this.createProductForm.value, this.product);
      product.idToRemove = this.getNotContained(this.product, this.createProductForm.value);
    }
    if (this.product) {
      this.inventoryService.updateProduct(product).subscribe((res) => {
        this.dataShareService.changeProductUpdated(product);
      });
    } else {
      this.inventoryService.saveProduct(product).subscribe((res) => {
        this.onReset();
      });
    }
  }

  getNotContained(toBeCompared, reference): string[] {
    const id2BeCompared = this.getAllIds(toBeCompared);
    const ref = this.getAllIds(reference);
    const notContained = this.getUnqiueIds(id2BeCompared, ref);
    return notContained;
  }

  getUnqiueIds(toFilter: string[], reference: string[]): string[] {
    return toFilter.filter((ele) => !reference.includes(ele));
  }

  getAllIds(product: Product): string[] {
    let ids = [];
    ids = product.productCode.filter((ele) => ele != null && ele !== '').map((ele) => ele.toUpperCase().trim());
    ids.push(product.barcode.toUpperCase().trim());
    return ids;
  }

  onReset(): void {
    this.createProductForm.removeControl('productCode');
    this.createProductForm.addControl('productCode', this.fb.array([this.fb.control('')]));
    this.createProductForm.get('productCode').reset();
    this.createProductForm.reset();
    this.formDirective.resetForm();
    this.firstInput.focus();
  }

  displayName(obj): string {
    return DisplayHelper.displayName(obj);
  }
}
