import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormArray, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Vendor } from 'app/models/Vendor';
import { InventoryService } from 'app/services/inventory.service';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-vendor-form',
  templateUrl: './vendor-form.component.html',
  styleUrls: ['./vendor-form.component.css'],
})
export class VendorFormComponent implements OnInit, OnChanges {
  get addresses() {
    return this.createVendorForm.get('addresses') as FormArray;
  }

  get phoneNumbers() {
    return this.createVendorForm.get('phoneNumbers') as FormArray;
  }

  get faxNumbers() {
    return this.createVendorForm.get('faxNumbers') as FormArray;
  }

  get emails() {
    return this.createVendorForm.get('emails') as FormArray;
  }

  constructor(private fb: FormBuilder, private inventoryService: InventoryService) {}
  @Input() vendor: Vendor;
  @Input() editMode: false;

  @ViewChild('formDirective') private formDirective: NgForm;
  @ViewChild('firstInput') private firstInput: MatInput;

  filteredOptions: Observable<Vendor[]>;

  createVendorForm = this.fb.group({
    _id: [],
    name: ['', Validators.required],
    addresses: this.fb.array([this.fb.control('')]),
    phoneNumbers: this.fb.array([this.fb.control('')]),
    faxNumbers: this.fb.array([this.fb.control('')]),
    emails: this.fb.array([this.fb.control('')]),
    paymentPeriodDays: [''],
  });

  addAddress() {
    this.addresses.push(this.fb.control(''));
  }

  addPhoneNumber() {
    this.phoneNumbers.push(this.fb.control(''));
  }

  addFaxNumber() {
    this.faxNumbers.push(this.fb.control(''));
  }

  addEmail() {
    this.emails.push(this.fb.control(''));
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'vendor': {
            for (let i = 0; i < this.vendor.addresses.length - 1; i++) {
              this.addAddress();
            }
            for (let i = 0; i < this.vendor.phoneNumbers.length - 1; i++) {
              this.addPhoneNumber();
            }
            for (let i = 0; i < this.vendor.faxNumbers.length - 1; i++) {
              this.addFaxNumber();
            }
            for (let i = 0; i < this.vendor.emails.length - 1; i++) {
              this.addEmail();
            }
            this.createVendorForm.patchValue(this.vendor);
          }
        }
      }
    }
  }

  ngOnInit(): void {
    this.filteredOptions = this.createVendorForm.get('name').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'vendor'))
    );
  }
  onReset() {
    this.createVendorForm.reset();
    this.formDirective.resetForm();
    this.firstInput.focus();
  }

  onSubmit() {
    if (this.vendor) {
      this.inventoryService.updateVendor(this.createVendorForm.value).subscribe((res) => {
        this.inventoryService.reFetchVendor();
      });
    } else {
      this.inventoryService.saveVendor(this.createVendorForm.value).subscribe((res) => {
        this.onReset();
        this.inventoryService.reFetchVendor();
      });
    }
  }

  onDelete() {
    this.inventoryService.deleteVendor(this.createVendorForm.value).subscribe((res) => {
      this.inventoryService.reFetchVendor();
    });
  }
}
