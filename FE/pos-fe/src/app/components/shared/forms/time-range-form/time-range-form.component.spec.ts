import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeRangeFormComponent } from './time-range-form.component';

describe('TimeRangeFormComponent', () => {
  let component: TimeRangeFormComponent;
  let fixture: ComponentFixture<TimeRangeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeRangeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeRangeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
