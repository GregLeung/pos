import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-time-range-form',
  templateUrl: './time-range-form.component.html',
  styleUrls: ['./time-range-form.component.css'],
})
export class TimeRangeFormComponent implements OnInit {
  @Output() parentOnSearch = new EventEmitter<any>();

  constructor(private fb: FormBuilder) {}

  timeRangeForm = this.fb.group({
    startDate: [moment(), Validators.required],
    endDate: [moment(), Validators.required],
  });

  ngOnInit(): void {}

  onSearch(): void {
    this.parentOnSearch.next(this.timeRangeForm.value);
  }
}
