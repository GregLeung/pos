import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.css'],
})
export class PaymentFormComponent implements OnInit, OnChanges {
  constructor() {}
  @Input() salesRecordType: string;
  @Input() paymentForm;
  @Output() parentNext = new EventEmitter<any>();

  readonly = false;

  get change() {
    return this.paymentForm.get('change');
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
        }
      }
    }
  }
}
