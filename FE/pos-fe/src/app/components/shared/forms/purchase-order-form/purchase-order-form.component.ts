import { DataShareService } from 'app/services/data-share.service';
import { InventoryService } from 'app/services/inventory.service';
import { Component, Input, OnInit, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, Validators, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Category } from 'app/models/Category';
import { Brand } from 'app/models/Brand';
import { Vendor } from 'app/models/Vendor';
import { Item } from 'app/models/Item';
import { PurchaseOrder } from 'app/models/PurchaseOrder';
import { DisplayHelper } from 'app/utils/displayHelper';
import { MatInput } from '@angular/material/input';
import { ItemTableComponent } from 'app/components/shared/item-table/item-table.component';
import { RequireMatch } from 'app/components/shared/forms/validators/selectFromDropdown';

@Component({
  selector: 'app-purchase-order-form',
  templateUrl: './purchase-order-form.component.html',
  styleUrls: ['./purchase-order-form.component.css'],
})
export class PurchaseOrderFormComponent implements OnInit, OnChanges {
  @Input() purchaseOrder: PurchaseOrder;
  @Input() readOnlyMode = false;

  @ViewChild('formDirective') private formDirective: NgForm;
  @ViewChild('firstInput') private firstInput: MatInput;
  @ViewChild(ItemTableComponent) private itemTableComponent: ItemTableComponent;

  items: any[] = [];

  filteredCategoryOptions: Observable<Category[]>;
  filteredVendorOptions: Observable<Vendor[]>;
  filteredBrandOptions: Observable<Brand[]>;

  vendorInfoForm = this.fb.group({
    vendor: ['', [Validators.required, RequireMatch]],
    vendorRefNum: ['', Validators.required],
    vendorOrderDate: ['', Validators.required],
    purchaser: ['', Validators.required],
    remark: [''],
    items: ['', Validators.required],
  });

  readyToSubmit = false;

  constructor(
    private fb: FormBuilder,
    private inventoryService: InventoryService,
    private dataShareService: DataShareService
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'purchaseOrder': {
            this.vendorInfoForm.patchValue(this.purchaseOrder);
            this.vendorInfoForm.disable();
            this.purchaseOrder.items.forEach((element) => {
              this.items.push(new Item(element));
            });
            break;
          }
        }
      }
    }
  }

  ngOnInit(): void {
    this.filteredVendorOptions = this.vendorInfoForm.get('vendor').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'vendor'))
    );
  }

  onResetVendorInfoForm(): void {
    this.vendorInfoForm.reset();
    this.vendorInfoForm.enable();
  }

  onConfirmVendorInfo(): void {
    this.vendorInfoForm.disable();
  }

  confirm(items: Item[]): void {
    this.readyToSubmit = true;
  }

  updateItems($event): void {
    this.vendorInfoForm.get('items').setValue($event);
    this.items = $event;
  }

  submit(): void {
    this.items.forEach((item) => {
      item.sn = item.sn.concat(item.originalSN);
      item.sn = item.sn.sort();
    });
    this.vendorInfoForm.get('items').setValue(this.items);

    const po: PurchaseOrder = new PurchaseOrder(this.vendorInfoForm.value);
    this.inventoryService.savePO(po).subscribe((res) => {
      this.onReset();
    });
  }

  onReset() {
    this.vendorInfoForm.reset();
    this.formDirective.resetForm();
    this.firstInput.focus();
    this.itemTableComponent.resetItemTable();
  }
  onDelete() {
    this.inventoryService.deletePO(this.purchaseOrder).subscribe((res) => {
      this.dataShareService.changePurchaseOrders([]);
    });
  }
  displayName(obj) {
    return DisplayHelper.displayName(obj);
  }
}
