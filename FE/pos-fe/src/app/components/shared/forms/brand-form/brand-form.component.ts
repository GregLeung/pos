import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, Validators, NgForm } from '@angular/forms';
import { Brand } from 'app/models/Brand';
import { InventoryService } from 'app/services/inventory.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-brand-form',
  templateUrl: './brand-form.component.html',
  styleUrls: ['./brand-form.component.css'],
})
export class BrandFormComponent implements OnInit, OnChanges {
  @Input() brand: Brand;
  @ViewChild('formDirective') private formDirective: NgForm;

  constructor(private fb: FormBuilder, private inventoryService: InventoryService) {}
  brandForm = this.fb.group({
    _id: [],
    name: ['', Validators.required],
  });
  filteredOptions: Observable<Brand[]>;
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'brand': {
            this.brandForm.get('_id').setValue(this.brand._id);
            this.brandForm.get('name').setValue(this.brand.name);
          }
        }
      }
    }
  }

  ngOnInit(): void {}
  onSubmit(): void {
    if (this.brand) {
      this.brandForm.get('_id').setValue(this.brand._id);
      this.inventoryService.updateBrand(this.brandForm.value).subscribe((res) => {
        this.inventoryService.reFetchBrand();
      });
    } else {
      this.inventoryService.saveBrand(this.brandForm.value).subscribe((res) => {
        this.onReset();
        this.inventoryService.reFetchBrand();
      });
    }
  }
  onReset(): void {
    this.brandForm.reset();
    this.formDirective.resetForm();
  }
}
