import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormControlBase } from 'app/models/config/formControlBase';

@Component({
  selector: 'app-dynamic-form-control',
  templateUrl: './dynamic-form-control.component.html',
  styleUrls: ['./dynamic-form-control.component.css'],
})
export class DynamicFormControlComponent implements OnInit {
  @Input() formControl: FormControlBase<any>;
  @Input() form: FormGroup;
  constructor() {}

  ngOnInit(): void {}
  get isValid() {
    return this.form.controls[this.formControl.key].valid;
  }
}
