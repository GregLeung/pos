import { DataShareService } from 'app/services/data-share.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-sales-record-action-dialog',
  templateUrl: './sales-record-action-dialog.component.html',
  styleUrls: ['./sales-record-action-dialog.component.css'],
})
export class SalesRecordActionDialogComponent implements OnInit {
  constructor(
    private dataShareService: DataShareService,
    public dialogRef: MatDialogRef<SalesRecordActionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.dataShareService.orderConvertedSource.subscribe((converted) => {
      if (Object.keys(converted).length > 0) {
        this.dialogRef.close();
      }
    });
  }
}
