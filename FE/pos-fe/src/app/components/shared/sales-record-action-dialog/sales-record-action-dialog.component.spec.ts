import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRecordActionDialogComponent } from './sales-record-action-dialog.component';

describe('SalesRecordActionDialogComponent', () => {
  let component: SalesRecordActionDialogComponent;
  let fixture: ComponentFixture<SalesRecordActionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRecordActionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRecordActionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
