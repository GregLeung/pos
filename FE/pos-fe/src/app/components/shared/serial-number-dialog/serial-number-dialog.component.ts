import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-serial-number-dialog',
  templateUrl: './serial-number-dialog.component.html',
  styleUrls: ['./serial-number-dialog.component.css'],
})
export class SerialNumberDialogComponent implements OnInit {
  sn: string;
  serialNumbers: string[];
  count: number;

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['sn', 'action'];

  constructor(
    public dialogRef: MatDialogRef<SerialNumberDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      count: number;
      serialNumbers: string[];
    }
  ) {
    this.count = data.count;
    this.serialNumbers = data.serialNumbers;
    this.count = this.count - this.serialNumbers.length;
    this.dataSource.data = this.serialNumbers;
  }

  ngOnInit(): void {}

  onConfirm(): void {
    this.dialogRef.close(this.serialNumbers);
  }

  onDismiss(): void {
    this.dialogRef.close(this.serialNumbers);
  }

  onAddSN(): void {
    if (this.count === 0 || this.serialNumbers.indexOf(this.sn) > -1) {
      return;
    }
    this.count -= 1;
    this.serialNumbers.push(this.sn);
    this.dataSource.data = this.serialNumbers;
    this.sn = '';
  }
  onRemove(index: number): void {
    this.serialNumbers.splice(index, 1);
    this.dataSource.data = this.serialNumbers;
  }
}
