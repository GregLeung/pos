import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit, OnChanges {
  @Input() salesRecord;

  change = 0;
  discount = 0;
  totalPaymentAmt = 0;
  totalPayable = 0;
  prevPaid = 0;
  paymentLabel = '';
  totalAmtLabel = '';
  payableLabel = '';
  reminderLabel = 'Change:';
  recordType = '';

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        this.recordType = this.salesRecord.recordType;
        switch (propName) {
          case 'salesRecord': {
            this.salesRecord.payments = this.salesRecord.payments.filter((ele) => ele.amt != null && ele.amt !== 0);
            if (this.salesRecord.recordType === 'return') {
              this.salesRecord.payments = this.salesRecord.payments.map((ele) => {
                ele.amt = ele.amt * -1;
                return ele;
              });
            }

            this.totalPaymentAmt = this.salesRecord.payments.reduce((accumulator, currentValue) => {
              return accumulator + currentValue.amt;
            }, 0);

            if (this.salesRecord.deposit) {
              this.prevPaid = this.salesRecord.deposit;
              this.totalPaymentAmt += this.prevPaid;
            }

            this.totalPayable = this.salesRecord.items
              .map((ele) => ele.qty * ele.adjustedPrice)
              .reduce((accumulator, currentValue) => {
                return accumulator + currentValue;
              }, 0);
            this.change = this.totalPaymentAmt - this.totalPayable;
            switch (this.salesRecord.recordType) {
              case 'order':
                this.paymentLabel = 'Deposit By:';
                this.totalAmtLabel = 'Deposit:';
                this.payableLabel = 'Payable:';
                if (this.change < 0) {
                  this.reminderLabel = 'Amt Due:';
                }
                return;
              case 'invoice':
                this.paymentLabel = 'Paid By:';
                this.totalAmtLabel = 'Paid:';
                this.payableLabel = 'Payable:';
                return;
              case 'return':
                this.paymentLabel = 'Refunded By:';
                this.payableLabel = 'Paid:';
                this.totalAmtLabel = 'Refunded:';
                return;
            }
            break;
          }
        }
      }
    }
  }
}
