import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedProductSearchDialogComponent } from './advanced-product-search-dialog.component';

describe('AdvancedProductSearchDialogComponent', () => {
  let component: AdvancedProductSearchDialogComponent;
  let fixture: ComponentFixture<AdvancedProductSearchDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedProductSearchDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedProductSearchDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
