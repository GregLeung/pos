import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SalesService } from 'app/services/sales.service';
import { InventoryService } from 'app/services/inventory.service';
import { Category } from 'app/models/Category';
import { Brand } from 'app/models/Brand';
import { Vendor } from 'app/models/Vendor';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { DisplayHelper } from 'app/utils/displayHelper';
import { RequireMatch } from 'app/components/shared/forms/validators/selectFromDropdown';

@Component({
  selector: 'app-advanced-product-search-dialog',
  templateUrl: './advanced-product-search-dialog.component.html',
  styleUrls: ['./advanced-product-search-dialog.component.css'],
})
export class AdvancedProductSearchDialogComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = [
    'barcode',
    'productCode',
    'desc',
    'brand',
    'category',
    'qty',
    'costIndividual',
    'costTen',
    'costHundred',
    'costThousand',
    'action',
  ];

  filteredCategoryOptions: Observable<Category[]>;
  filteredVendorOptions: Observable<Vendor[]>;
  filteredBrandOptions: Observable<Brand[]>;

  searchForm = this.fb.group({
    brand: ['', RequireMatch],
    category: ['', RequireMatch],
    description: [''],
  });

  constructor(
    private salesService: SalesService,
    private inventoryService: InventoryService,
    public dialogRef: MatDialogRef<AdvancedProductSearchDialogComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.filteredBrandOptions = this.searchForm.get('brand').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'brand'))
    );
    this.filteredCategoryOptions = this.searchForm.get('category').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'cat'))
    );
    this.dataSource.data = [];
  }

  onSelectAll(): void {
    this.dialogRef.close(this.dataSource.data);
  }
  onSelected(): void {
    this.dialogRef.close(this.selection.selected);
  }
  onDismiss(): void {
    this.dialogRef.close();
  }
  onSearch(): void {
    this.salesService.getProducts(this.searchForm.value).subscribe((res) => {
      this.dataSource.data = res;
    });
  }
  getCsv(): void{
    this.salesService.getProductListCsv(this.searchForm.value);
  }
  onRemove(index: number): void {}
  displayHelper(obj) {
    return DisplayHelper.displayName(obj);
  }
}
