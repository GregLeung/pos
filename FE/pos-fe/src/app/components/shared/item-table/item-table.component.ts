import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  DoCheck,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Item } from 'app/models/Item';
import { SalesService } from 'app/services/sales.service';
import { DataShareService } from 'app/services/data-share.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AdvancedProductSearchDialogComponent } from 'app/components/shared/advanced-product-search-dialog/advanced-product-search-dialog.component';
import { SerialNumberDialogComponent } from 'app/components/shared/serial-number-dialog/serial-number-dialog.component';
import { INFO } from 'app/models/config/compInfo';
import { MatInput } from '@angular/material/input';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-item-table',
  templateUrl: './item-table.component.html',
  styleUrls: ['./item-table.component.css'],
})
export class ItemTableComponent implements OnInit, OnChanges {
  @Output() updateParentItems = new EventEmitter<Item[]>();
  @Input() itemsInput: any[];
  @Input() readOnlyMode = false;
  @Input() poEntryMode = false;

  @ViewChild('firstInput') private firstInput: MatInput;
  @ViewChild('itemForm') private itemForm: NgForm;

  priceSecret: string;
  compNameArr: string[];

  displayedColumns: string[] = [
    'title',
    'desc',
    'adjustedPrice',
    'qty',
    'discount',
    'price',
    'totalDiscount',
    'netTotal',
    'action',
  ];
  displayedColumnsFooter: string[] = [
    'title',
    'desc',
    'adjustedPrice',
    'qty',
    'discount',
    'price',
    'totalDiscount',
    'netTotal',
    'action',
  ];
  dataSource = new MatTableDataSource<any>();
  barcodeToSearch: string;
  disabling: boolean;

  items = [];

  onSearch(): void {
    if (this.barcodeToSearch === '') {
      return;
    }
    this.barcodeToSearch = this.barcodeToSearch.toUpperCase();
    this.salesService.getProduct(this.barcodeToSearch).subscribe((product) => {
      const temp: Item = new Item(product);
      this.addItem(temp);
    });
    this.barcodeToSearch = '';
  }

  constructor(
    private salesService: SalesService,
    private dataShareService: DataShareService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.compNameArr = INFO.PRICE_SECRET.split('0');
    this.priceSecret = '';
    this.disabling = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'itemsInput': {
            this.items = this.itemsInput;
            this.dataSource.data = this.itemsInput;
            this.focusBarcodeInputField();
            break;
          }
        }
      }
    }
  }

  onRemove(index: number): void {
    const temp: Item[] = this.dataSource.data;
    temp.splice(index, 1);
    this.dataSource.data = temp;
  }

  addSerialNumberList(element: any, count: number): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '50%';
    dialogConfig.data = { count: count, serialNumbers: element.sn };
    const dialogRef = this.dialog.open(SerialNumberDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        element.sn = value;
      }
    });
  }

  getTotalCost() {
    const iterator = this.dataSource.data.values();
    let total = 0;
    for (const item of iterator) {
      total += item.qty * item.cost;
    }
    return total;
  }

  getTotalPrice() {
    const iterator = this.dataSource.data.values();
    let total = 0;
    for (const item of iterator) {
      total += item.qty * item.adjustedPrice;
    }
    return total;
  }

  addItem(item: Item): void {
    this.showSecretTemporarily(item);
    let itemExists = false;
    for (const i of this.items) {
      if (i.barcode === item.barcode) {
        i.qty++;
        itemExists = !itemExists;
        break;
      }
    }
    if (!itemExists) {
      item.qty = 1;
      this.items.push(item);
    }
    this.dataSource.data = this.items;
    this.updateParentItems.next(this.items);
  }

  onAdvancedSearch(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = '90%';
    dialogConfig.minWidth = '90%';
    const dialogRef = this.dialog.open(AdvancedProductSearchDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((value) => {
      if (value) {
        value.forEach((element) => {
          const temp = new Item(element);
          this.addItem(temp);
        });
      }
    });
  }

  showSecretTemporarily(item: Item): void {
    let priceTemp = item.cost.toString();
    priceTemp = priceTemp.padStart(4, '0');
    let temp = '';
    this.priceSecret = '';
    for (let i = 0; i < priceTemp.length; i++) {
      temp += this.compNameArr[i] + priceTemp[i];
    }
    temp += this.compNameArr[4];
    this.priceSecret = temp;
    setTimeout(() => {
      this.priceSecret = '';
    }, 3 * 1000);
  }

  resetItemTable(): void {
    this.items = [];
    this.dataSource.data = this.items;
  }

  focusBarcodeInputField(): void {
    if (this.firstInput) {
      this.firstInput.focus();
    }
  }

  getItemForm(): any {
    return this.itemForm;
  }
}
