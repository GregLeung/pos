import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.css'],
})
export class PasswordDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<PasswordDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  password: string;

  ngOnInit(): void {}
  onConfirm(): void {
    if (this.password === '69773937') {
      this.dialogRef.close(true);
    }
  }
  onDismiss(): void {
    this.dialogRef.close(false);
  }
}
