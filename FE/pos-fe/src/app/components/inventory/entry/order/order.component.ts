import { InventoryService } from 'app/services/inventory.service';
import { DataShareService } from 'app/services/data-share.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Category } from 'app/models/Category';
import { Brand } from 'app/models/Brand';
import { Vendor } from 'app/models/Vendor';
import { Item } from 'app/models/Item';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {
  items: Item[];

  filteredCategoryOptions: Observable<Category[]>;
  filteredVendorOptions: Observable<Vendor[]>;
  filteredBrandOptions: Observable<Brand[]>;

  vendorInfoForm = this.fb.group({
    vendor: ['', Validators.required],
    vendorRefNum: ['', Validators.required],
    vendorOrderDate: ['', Validators.required],
    purchaser: ['', Validators.required],
    remark: [''],
  });

  readyToSubmit = false;

  constructor(
    private fb: FormBuilder,
    private dataShareService: DataShareService,
    private inventoryService: InventoryService
  ) {}

  ngOnInit(): void {
    this.dataShareService.currentItems.subscribe((items) => (this.items = items));

    this.filteredVendorOptions = this.vendorInfoForm.get('vendor').valueChanges.pipe(
      startWith(''),
      map((value) => this.inventoryService._filter(value, 'vendor'))
    );
  }

  onResetVendorInfoForm(): void {
    this.vendorInfoForm.reset();
    this.vendorInfoForm.enable();
  }

  onConfirmVendorInfo(): void {
    this.vendorInfoForm.disable();
  }

  confirm(): void {
    this.readyToSubmit = true;
  }
}
