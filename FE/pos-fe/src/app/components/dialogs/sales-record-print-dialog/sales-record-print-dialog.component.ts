import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-sales-record-print-dialog',
  templateUrl: './sales-record-print-dialog.component.html',
  styleUrls: ['./sales-record-print-dialog.component.css'],
})
export class SalesRecordPrintDialogComponent implements OnInit {
  displayedColumns: string[] = ['barcode', 'name', 'desc', 'qty', 'price', 'amt'];
  footerColumns: string[] = ['barcode', 'name', 'desc', 'qty', 'price', 'amt'];

  cssUrl: string;

  constructor(
    public dialogRef: MatDialogRef<SalesRecordPrintDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    if (this.data.salesRecord.items.length > 10) {
      this.cssUrl = '/assets/styleSheets/a4Receipt.css';
    } else {
      this.cssUrl = '/assets/styleSheets/a5Receipt.css';
    }
  }

  print(): void {
    window.print();
    this.dialogRef.close();
  }
}
