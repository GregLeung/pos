import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRecordPrintDialogComponent } from './sales-record-print-dialog.component';

describe('SalesRecordPrintDialogComponent', () => {
  let component: SalesRecordPrintDialogComponent;
  let fixture: ComponentFixture<SalesRecordPrintDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRecordPrintDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRecordPrintDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
